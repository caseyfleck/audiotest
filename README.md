This is a test project that accepts a UDP audio stream from the ASR microservice and allows you to hear the stream over your speakers. Its data is in the raw format, much like that from a microphone.  This is _not_ production code 
and is only intended as a test. It can be deleted when it is no longer useful.

For this to work with the ASR microservice, you MUST add your IP:userID to the parameter 'test.send-audio-to-ip' in /opt/cc/microservices/asr/features.conf on the ASR dev (or other target) server. In addition, you must make sure both 
'test.test-mode-enabled' and 'test.enable-audio-to-ip' are set to 'true' in the same file.

The port 55555 is hard-coded here, but you will have to switch out your own VPN IP below.

Created by Brent Wagenseller, August 2020.