package com.casey;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
/*
This is the client, which transforms the microphone data to a stream and then sends to the server.
*/
public class UDPClientSender {
    public static void main(String[] args) throws IOException {
        //AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100, 16, 2, 4, 44100, true);//initial reccommendation
        //AudioFormat format = new AudioFormat(16000, 16, 1, true, false); //use for mic testing ONLY
        AudioFormat format = new AudioFormat(8000, 16, 1, true, false); //use for mic testing ONLY
        TargetDataLine microphone;
        try {
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            microphone = (TargetDataLine) AudioSystem.getLine(info);
            microphone.open(format);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int numBytesRead;
            int CHUNK_SIZE = 1024;
            byte[] data = new byte[microphone.getBufferSize() / 5];
            microphone.start();
            // Configure the ip and port
            String hostname = "10.248.128.21";
            //String hostname = "127.0.0.1";//"localhost";
            int port = 55555;
            InetAddress address = InetAddress.getByName(hostname);
            DatagramSocket socket = new DatagramSocket();
            for(;;) {
                numBytesRead = microphone.read(data, 0, CHUNK_SIZE);
                // write the mic data to a stream for use later
                out.write(data, 0, numBytesRead);
                DatagramPacket request = new DatagramPacket(data,numBytesRead, address, port);
                socket.send(request);
            }
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }
}