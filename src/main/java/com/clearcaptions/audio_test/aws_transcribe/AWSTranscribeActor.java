package com.clearcaptions.audio_test.aws_transcribe;

import akka.actor.*;
import akka.util.ByteString;
import com.clearcaptions.audio_test.config.AsrConfig;
import com.clearcaptions.audio_test.internal_msgs.ActorSinkMsgs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This actor is launched by ASRCaptionSessionActor when a caption session requires an AWS Transcribe agent.
 *
 * This actor doubles as a sink for the UDP stream - see stream.UDPStream for more information, but this actor is required to respond to the following messages
 *    * ByteString.class (which is the actual payload from the stream)
 *    * ActorSinkMsgs.StreamInitialized.class
 *    * ActorSinkMsgs.StreamCompleted.class
 *    * ActorSinkMsgs.StreamFailure.class
 *
 * This actor sends these messages back to the stream:
 *    * ActorSinkMsgs.Ack.ACKNOWLEDGE - This is sent to the stream as a way of simulating backpressure in the stream; it MUST be done after EVERY message is received from the  stream, otherwise
 *          backpressure will NOT work properly!
 *
 * These messages are received from the parent:
 *   * ASRv2ClusterCommMsg.EndASRCaptionSession - When the ASRCaptionSessionActor is told the caption session is ending, it sends a ASRv2ClusterCommMsg.EndASRCaptionSession message to all its children,
 *          indicating that they should shut down themselves. The child actor responds to the parent with a ASRv2InternalMsg.VendorCaptionEnd (when all vendors respond with a
 *          ASRv2InternalMsg.VendorCaptionEnd, the parent performs QA duties).
 *
 * These messages are sent to its parent ASRCaptionSessionActor:
 *   * ASRv2InternalMsg.VendorCaptionStart - tell the parent ASRCaptionSessionActor that the actor officially launched with its start time.
 *   * ASRv2InternalMsg.VendorCaptionEnd - This is sent to the parent ASRCaptionSessionActor when the vendor session is ending. This is sent in the postStop(), and is in response to a
 *          PoisonPill.getInstance() from the parent ASRCaptionSessionActor. This is important to send to the parent ASRCaptionSessionActor, as this signals to the parent actor that its time to evaluate
 *          the caption quality (the parent must know the child is finished before this can happen).
 *
 * These messages are sent to the statistic actor(s):
 *   * ASRv2StatisticMsg.IncreaseAWSTranscribeActorCountMsg - This message is sent to both the main statistic actor as well as the corresponding Asterisk statistic actor when this actor starts; each
 *          statistic actor increases the associated counter by 1.
 *   * ASRv2StatisticMsg.DecreaseAWSTranscribeActorCountMsg - This message is sent to both the main statistic actor as well as the corresponding Asterisk statistic actor when this actor ends; each
 *          statistic actor increases the associated counter by 1.
 *   * ASRv2StatisticMsg.AWSTranscribeActorRestart - This message is sent to both the main statistic actor as well as the corresponding Asterisk statistic actor if this actor experiences an error that
 *          forces it to restart; each statistic actor increases the associated counter by 1.
 *
 * This actor launches these actors:
 *   * AWSWorker - the worker actor that will actually interact with Amazon. Each instance of AWSTranscribeActor launches exactly one of these actors.
 */
public class AWSTranscribeActor extends AbstractActor {

    private static final Logger logger = LoggerFactory.getLogger(AWSTranscribeActor.class);

    // Each sink needs its own PipedOutputStream and PipedInputStream - these actually form a second simple stream (in addition to the target UDP stream), and their purpose is to
    // populate the stream used in an AudioInputStream object. This stream is populated by handleIncomingPacket, and is eventually used as the input to the AudioInputStream.
    private PipedOutputStream pipedOut= new PipedOutputStream();
    private PipedInputStream pipedIn;

    // The audio format this instance will use
    private AudioFormat audioFormat;

    // The audio stream which is sent to AWS Transcribe
    private AudioInputStream audio;

    // Since this actor will only last a few minutes, this config actor should not change
    private AsrConfig config;

    // We want to keep track of how many buffer dumps happen - if it crosses a certain threshold, we want to be notified
    private long bufferDumps;

    ActorRef awsWorker;

    /*
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     ************************************************************CONSTRUCTORS and Props************************************************************
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     */
    /**
     * The constructor.
     *
     * @param config The config object.
     */
    public AWSTranscribeActor(AsrConfig config) {
        this.config = config;

        this.bufferDumps = 0;

        //open this sink's own personal stream for the data.
        try {
            pipedIn = new PipedInputStream(pipedOut, this.config.getAudioFormatInputStreamPipeSize());
        } catch (Exception e) {
            logger.error("Problem adding PipedInputStream. Captions will NOT work for AWS Transcribe for this session!");
        }

        this.audioFormat = new AudioFormat(this.config.getAudioFormatSampleRate(), this.config.getAudioFormatSampleSizeInBits(), this.config.getAudioFormatChannels(), this.config.getAudioFormatPcmSigned(), this.config.getAudioFormatBigEndian());

        //build the AudioInputStream
        try {
            audio = new AudioInputStream(pipedIn, audioFormat, AudioSystem.NOT_SPECIFIED);
        } catch (Exception e) {
            logger.error("AWSTranscribeActor [{}] could not open audio input stream. Message: [{}].", self().path().name(), e.getMessage(),e);
        }

    }

    /**
     * The props.
     */
    public static Props props(AsrConfig config) {
        return Props.create(AWSTranscribeActor.class, () -> new AWSTranscribeActor(config));
    }

    /*
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     ************************************************************Akka Actor Overrides************************************************************
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     */

    @Override
    public void preStart() {
        logger.debug("Startup AWSTranscribeActor actor with name [{}].", self().path().name());

        long bornOnTime = System.currentTimeMillis();

         // launch the AWS worker, which immediately begins captioning!
        awsWorker = context().actorOf(AWSWorker.props(config, audio, self().path().name()), "my.AWSWorker");
    }

    @Override
    public void postStop() {
        logger.debug("Stopping AWSTranscribeActor actor with name [{}].", self().path().name());

        long timeOfDeath = System.currentTimeMillis();

        // close the PipedInputStream and PipedOutputStream
        try {
            pipedIn.close();
        } catch (Exception e) {
            logger.warn("postStop: could not close InputStream pipe for AWS Transcribe actor [{}].", self().path().name(), e);
        }
        try {
            pipedOut.close();
        } catch (Exception e) {
            logger.warn("postStop: could not close OutputStream pipe for AWS Transcribe actor [{}].", self().path().name(), e);
        }
        //try to close the audioInputStream
        try {
            audio.close();
        } catch (Exception e) {
            logger.warn("postStop: could not close audio connection for AWS Transcribe actor [{}].", self().path().name(), e);
        }

    }

    @Override
    public void preRestart(Throwable reason, Optional<Object> message) throws Exception {
        logger.error("preRestart: AWS Transcribe Actor {} has encountered some catastrophic error and had to be re-started in method {}.", self().path().name(), message.get(), reason);
    }

    /*
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     ************************************************************Akka Receive Handlers************************************************************
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     */

    /**
     * This method simply acts as a receiver for messages we know we will get but do not care to do anything with.
     * @param msg The received message.
     */
    private void doNothing(Object msg) {

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This handles the actual payload from the stream.
     * @param packet A ByteString that is the payload.
     */
    private void handleIncomingPacket(ByteString packet) {

        int inputStreamPipeSize = config.getAudioFormatInputStreamPipeSize();
        byte[] data;
        byte[] rawData = packet.toArray();
        byte[] trashBytes = new byte[inputStreamPipeSize];
        int trashInt; // not actually used, only set- this is needed if we have to dump a buffer

        // we do not have to do anything like ripping rtp headers off or decoding from ulaw to pcmu - we let the asr microservice do this and then it sends the raw data
        // to us, so we do not have to do any postprocessing of the data
        data = rawData;

        try {
            // The 'available()' method displays the number of bytes in the pipedIn buffer; if we try to write more data to the buffer than it can hold (capped at inputStreamPipeSize),
            // This will block. we need to completely dump the data if this cap is about to be hit.
            // Technically, the data is read into trashBytes, with trashInt as the number of bytes read - but we just discard them immediately without doing anything with them.
            if ((pipedIn.available() + data.length) > inputStreamPipeSize) {
                trashInt = pipedIn.read(trashBytes);
            }

            pipedOut.write(data);
        } catch (Exception e) { }

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This method is launched if there is an error in the stream.
     * @param failure A throwable failure.
     */
    private void handleStreamError(ActorSinkMsgs.StreamFailure failure) {
        logger.error("Error in stream: [{}].", failure.getCause(), failure);

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }



    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(ByteString.class, msg -> handleIncomingPacket(msg))
                .match(ActorSinkMsgs.StreamInitialized.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamCompleted.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamFailure.class, msg -> handleStreamError(msg))
                .build();
    }

    /*
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     ************************************************************Static Methods************************************************************
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     */

    /**
     * Get the prefix for this type of actor.
     *
     * @return The prefix for this actor.
     */
    public static String getActorPrefix() { return "ASRAWS"; }

    /**
     * Accepts a caption bridge ID, strips off the captionBridgeID prefix, and then returns an appropriate name for this actor.
     *
     * @param captionBridgeID The associated captionBridge ID in Asterisk for this caption session.
     * @return The proposed name for this actor.
     */
    public static String getProposedActorName(String captionBridgeID) { return getActorPrefix() + captionBridgeID.substring(2); }
}