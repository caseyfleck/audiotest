package com.clearcaptions.audio_test.aws_transcribe;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.transcribestreaming.model.AudioEvent;
import software.amazon.awssdk.services.transcribestreaming.model.AudioStream;

/**
 * The base of this class is directly from the bidirectional AWS Transcribe example.
 *
 * The AWS Transcribe component uses an AudioStreamPublisher - unfortunately it's on the programmer to write this object. AWS does give a base example, but it uses threading to achieve its goal -
 * and unfortunately, whatever actor that defines the AudioStreamPublisher object becomes completely unresponsive (in our case, the child actor AWSWorker is the sacrificial actor), as AWS runs its
 * request() method constantly and it seems to block out most other calls. AWS does provide an override cancel() method in the class that implements 'Subscription', but its hard to call this as the
 * request() thread hammers it (AND it has a questionably long do...while loop).
 *
 * Since we cannot _reliably_ get to the cancel() method in the subclass that implements Subscription, we can maintain a thread-safe AtomicBoolean in a class that is _not_ stun-locked by
 * AudioStreamPublisher - in our case, AWSTranscribeActor maintains the AtomicBoolean 'keepRunning', whose reference is passed to the AWSWorker and then the AudioStreamPublisher.  When keepRunning
 * is set to 'false' in AWSTranscribeActor, it flows to the AudioStreamPublisher which checks the value in multiple spots and shuts down if there it is suddenly set to 'false'.
 *
 */

public class AudioStreamPublisher implements Publisher<AudioStream> {
    private final InputStream inputStream;

    private static final Logger logger = LoggerFactory.getLogger(AudioStreamPublisher.class);

    // The associated awsTranscribeActorName
    private String awsTranscribeActorName;

    // the number of AudioStreamPublishers the associated AWSWorker started. This is this instances' number
    private int sessionNum;

    public AudioStreamPublisher(InputStream inputStream, String awsTranscribeActorName, int sessionNum) {

        this.inputStream = inputStream;
        this.awsTranscribeActorName = awsTranscribeActorName;

        this.sessionNum = sessionNum;
    }

    @Override
    public void subscribe(Subscriber<? super AudioStream> s) {
        s.onSubscribe(new SubscriptionImpl(s, inputStream));
    }

    private class SubscriptionImpl implements Subscription {
        private static final int CHUNK_SIZE_IN_BYTES = 1024 * 1;
        private ExecutorService executor = Executors.newFixedThreadPool(1);
        private AtomicLong demand = new AtomicLong(0);

        private final Subscriber<? super AudioStream> subscriber;
        private final InputStream inputStream;

        private SubscriptionImpl(Subscriber<? super AudioStream> s, InputStream inputStream) {
            this.subscriber = s;
            this.inputStream = inputStream;
        }

        @Override
        public void request(long n) {
            if (n <= 0) {
                subscriber.onError(new IllegalArgumentException("Demand must be positive"));
            }

            demand.getAndAdd(n);

            executor.submit(() -> {
                /*
                It should be noted that this is not simply entered _once_ - its entered MULTIPLE TIMES A SECOND, and this can happen even after pipedIn is terminated.
                 */

                // if keepRunning was NOT marked as false in AWSTranscribeActor, continue

                try {

                    do {
                        ByteBuffer audioBuffer = getNextEvent();
                        if (audioBuffer.remaining() > 0) {
                            AudioEvent audioEvent = audioEventFromBuffer(audioBuffer);
                            subscriber.onNext(audioEvent);
                        } else {
                            subscriber.onComplete();
                            break;
                        }
                    } while (demand.decrementAndGet() > 0);

                } catch (java.io.UncheckedIOException e) {
                    if ((e.getMessage() != null) && ((e.getMessage().contains("Pipe closed")) || (e.getMessage().contains("Pipe broken")) || (e.getMessage().contains("Write end dead")))) {
                        // if the error is 'pipe closed' / 'Pipe broken' / 'Write end dead', we know this is due to us not properly handling the end of the stream - so we do not need to capture
                        // the stack trace
                        // ('pipe closed' / 'Pipe broken' seem to happen at the end of the call, abd 'Write end dead' seems to happen on fast busies)
                        String specificMessage = (e.getMessage().contains("Pipe closed") ? "Pipe closed" :
                                ((e.getMessage().contains("Pipe broken")) ? "Pipe broken" :
                                        ((e.getMessage().contains("Write end dead")) ? "Write end dead" : "UNKNOWN")));
                        logger.warn("request: The stream pipe was improperly closed for the AWSWorker actor with general message [{}] associated with AWSTranscribeActor [{}], session number [{}].", specificMessage, awsTranscribeActorName, sessionNum);
                    } else {
                        logger.error("request: there was an error with the audio buffer of size {} for AWSTranscribeActor [{}], session number [{}].", n, awsTranscribeActorName, sessionNum, e);
                    }
                } catch (Exception e) {

                    logger.error("request: there was an error with the audio buffer of size {} for AWSTranscribeActor [{}], session number [{}].", n, awsTranscribeActorName, sessionNum, e);
                    subscriber.onError(e);
                }
            });
        }

        @Override
        public void cancel() {
            logger.info("AudioStreamPublisher associated with AWSTranscribeActor [{}], session number [{}], shutting down gracefully.", awsTranscribeActorName, sessionNum);
            subscriber.onComplete();
        }

        private ByteBuffer getNextEvent() {
            ByteBuffer audioBuffer;
            byte[] audioBytes = new byte[CHUNK_SIZE_IN_BYTES];

            int len = 0;
            try {
                len = inputStream.read(audioBytes);

                if (len <= 0) {
                    audioBuffer = ByteBuffer.allocate(0);
                } else {
                    audioBuffer = ByteBuffer.wrap(audioBytes, 0, len);
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }

            return audioBuffer;
        }

        private AudioEvent audioEventFromBuffer(ByteBuffer bb) {
            return AudioEvent.builder()
                    .audioChunk(SdkBytes.fromByteBuffer(bb))
                    .build();
        }
    }
}
