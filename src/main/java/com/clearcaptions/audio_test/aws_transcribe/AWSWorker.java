package com.clearcaptions.audio_test.aws_transcribe;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.clearcaptions.audio_test.config.AsrConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.profiles.ProfileFile;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.transcribestreaming.TranscribeStreamingAsyncClient;
import software.amazon.awssdk.services.transcribestreaming.model.*;
import javax.sound.sampled.AudioInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Unfortunately, once we run 'client.startStreamTranscription(request, publisher, response).join()' for the AWS Transcribe connection, the actor loses ALL ability to receivev messages;
 * This means we cannot interact with this actor at all once the stream starts.
 *
 * For now, we simply placed this code in a child actor of AWSTranscribeActor, in the hopes that when AWSTranscribeActor shuts down, this actor will too (it seems AWSTranscribeActor
 * can still receive messages and shut down when this child actor is running).
 *
 * This is not the best solution, but its a solution for now until we can figure this out. Once we figure out how to properly deal with this issue, this class should be destroyed
 * and folded into AWSTranscribeActor.
 *
 * This actor also interacts with an AWS object that internally interacts with AWS, sending audio to AWS and receiving captions back in return.
 *
 * These messages are sent to its parent ASRCaptionSessionActor:
 *   * ASRv2InternalMsg.BatchCaption - A batch caption is the translated caption received from AWS. When this message is received from AWS, this actor makes a ASRv2InternalMsg.BatchCaption message
 *          and then sends it to its (grand)parent ASRCaptionSessionActor (so it can record it for QA and then send the transcribed captions back to the ACD).
 *
 * These message are sent to itself:
 *   * ReStartSession (Internal class) - This is use to re-start the session after a timeout. Unfortunately AWS does not allow for the timeout to be set by us, so we have to re-establish the connection.
 *          We do track this to use for our own timeout tracking.
 *          This is done via messaging and NOT recursion simply so the connection can _completely_ clear out and is not left in memory (i.e. the last call to beginCaptioning() is completely cleared
 *          from memory).
 */

public class AWSWorker extends AbstractActor {

    private static final Logger logger = LoggerFactory.getLogger(AWSWorker.class);

    //This tracks the batch number sent for the captions. only increases by 1 each time a caption is sent
    private int transmissionNum;

    // The audio stream which is sent to Watson
    private AudioInputStream audio;

    private AsrConfig config;

    //The name of AWSWorker's direct parent (to print in error logs if need be)
    private String awsTranscribeActorName;

    //this message will be sent to this class if it needs to re-start due to a timeout
    private class ReStartSession {}

    //track how many sessions there are
    int sessionNum;

    /*
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     ************************************************************CONSTRUCTORS and Props************************************************************
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     */

    /**
     * The constructor.
     *
     * @param config The config object.
     * @param audio The parent's pre-opened AudioInputStream. The parent will manage and feed this stream.
     */
    public AWSWorker(AsrConfig config, AudioInputStream audio, String awsTranscribeActorName) {

        this.config = config;
        this.awsTranscribeActorName = awsTranscribeActorName;

        this.transmissionNum = 0;

        sessionNum = 0;

        this.audio = audio;
    }

    /**
     * The props
     */
    public static Props props(AsrConfig config, AudioInputStream audio, String awsTranscribeActorName) {
        return Props.create(AWSWorker.class, () -> new AWSWorker(config, audio, awsTranscribeActorName));
    }

    /*
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     ************************************************************Akka Actor Overrides************************************************************
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     */

    @Override
    public void preStart() {
        logger.info("Startup AWSWorker actor with name [{}].", self().path().name());

        // immediately begin captioning!
        this.beginCaptioning();
    }

    @Override
    public void preRestart(Throwable reason, Optional<Object> message) throws Exception {
        logger.warn("AWSWorker actor [{}] restarted with message [{}].", self().path().name(), reason.getMessage(), reason);
    }

    @Override
    public void postStop() {

        logger.debug("Stopping AWSWorker actor [{}].", self().path().name());
        // we do not have to close the PipedInputStream we got from the parent - the parent will handle that!
    }

    /*
     *************************************************************************************************************************************
     *************************************************************************************************************************************
     ************************************************************Class Methods************************************************************
     *************************************************************************************************************************************
     *************************************************************************************************************************************
     */

    /**
     * This method begins the captioning session with AWS Transcribe.
     *
     */
    public void beginCaptioning() {

        sessionNum++;

        AudioStreamPublisher publisher = null;
        TranscribeStreamingAsyncClient client = null;
        StartStreamTranscriptionRequest request = null;
        StartStreamTranscriptionResponseHandler response = null;

        logger.info("AWS Transcribe worker actor [{}] and session number [{}], is now providing captions!", self().path().name(), sessionNum);

        publisher = new AudioStreamPublisher(audio, awsTranscribeActorName, sessionNum);

        //build the client
        //Since we do not have our credentials in ~/.aws we have to do this
        // Note that, unfortunately, we cannot handle config entry 'output = json' - that said, that may be the default
        Path credentialPath = Paths.get(config.getAwsCredentialFile());
        ProfileFile pf =  ProfileFile.builder().content(credentialPath).type(ProfileFile.Type.CREDENTIALS).build();
        ProfileCredentialsProvider profileCredentialsProvider = ProfileCredentialsProvider.builder().profileFile(pf).build();

        try {
            client = TranscribeStreamingAsyncClient.builder().credentialsProvider(profileCredentialsProvider).region(Region.of(config.getAwsRegion())).build();

            // If the credentials are in ~/.aws, you can use the line below for  the credentials (which does _both_ credentials and config)
            //client = TranscribeStreamingAsyncClient.builder().credentialsProvider(ProfileCredentialsProvider.create()).build();
        } catch (Exception e) {
            logger.error("AWS Transcribe worker actor [{}] could not build the client. Message: [{}].", self().path().name(), e.getMessage(),e);
        }

        //build the request
        try {
            request = StartStreamTranscriptionRequest.builder()
                    .mediaEncoding(MediaEncoding.PCM)
                    .languageCode("en-US") // en-US, de-DE, it-IT, fr-FR, en-AU, es-US, fr-CA, en-GB. can also use the enums in LanguageCode.XX_XX (ex - LanguageCode.EN_US)
                    .mediaSampleRateHertz(config.getAudioFormatSampleRate())
                    .build();
        } catch (Exception e) {
            logger.error("AWS Transcribe worker actor [{}] could not build the request. Message: [{}].", self().path().name(), e.getMessage(),e);
        }

        try {

            response =
                    StartStreamTranscriptionResponseHandler.builder().subscriber(e -> {
                        //everything in this temp method will run when a 'TranscriptEvent' occurs
                        TranscriptEvent event = (TranscriptEvent) e;
                        event.transcript().results()
                                .forEach(r -> {
                       /*
                       r class: software.amazon.awssdk.services.transcribestreaming.model.Result
                       The resultID can be accessed with r.resultId(), and it changes with EVERY sentence
                       You can know if its a partial with r.isPartial(); == False means Amazon is finished with interpreting the sentence

                       */
                                    if (r.isPartial() == false) {
                                        r.alternatives().forEach(a -> {
                               /*
                               'a' is of class software.amazon.awssdk.services.transcribestreaming.model.Alternative
                               You can find the number of words in the sentence with a.items().size()

                               */

                                            //a.items().forEach(x-> {
                                            //    logger.error("BRENT TAKE THIS OUT: {}.", x.toString());
                                            //});

                                            logger.info("AWS transcript: [{}]", a.transcript());
                                            this.transmissionNum++;
                                        });
                                    }
                                });
                    })
                            .build();
        } catch (Exception e) {
            logger.error("AWS Transcribe worker actor [{}] could not build the response. Message: [{}].", self().path().name(), e.getMessage(),e);
        }

        try {
            //this line takes over the actor thread itself - it will go until it errors out, so currently, ALL passes through this will hit the catch()
            client.startStreamTranscription(request, publisher, response).join();
        } catch (Exception e) {
            logger.error("AWS Transcribe worker actor [{}] could not start the stream to AWS. Message: [{}].", self().path().name(), e.getMessage(),e);
        }
    }

    /*
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     ************************************************************Akka Receive Handlers************************************************************
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     */

    public void handleUnknownMessage(Object msg) {
        logger.warn("Received unknown message: [{}]", msg.getClass());
    }

    /**
     * This actor should not get any messages, as all messages are blocked by AWS.
     */
    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .matchAny(msg -> handleUnknownMessage(msg))
                .build();
    }

    /*
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     ************************************************************Static Methods************************************************************
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     */

    /**
     * Get the prefix for this type of actor.
     *
     * @return The prefix for this actor.
     */
    public static String getActorPrefix() { return "ASRAWSWORK"; }

    /**
     * Accepts a caption bridge ID, strips off the captionBridgeID prefix, and then returns an appropriate name for this actor.
     *
     * @param captionBridgeID The associated captionBridge ID in Asterisk for this caption session.
     * @return The proposed name for this actor.
     */
    public static String getProposedActorName(String captionBridgeID) { return getActorPrefix() + captionBridgeID.substring(2); }
}
