package com.clearcaptions.audio_test.old_simple_version;

import java.io.File;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import javax.sound.sampled.*;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.alpakka.udp.Datagram;
import akka.stream.alpakka.udp.javadsl.Udp;
import akka.stream.javadsl.*;
import akka.util.ByteString;

/**
This is a test project that accepts a UDP audio stream from the ASR microservice and allows you to hear the stream over your speakers. Its data is in the raw format, much like
 that from a microphone.  This is _not_ production code and is only intended as a test. It can be deleted when it is no longer useful.

For this to work with the ASR microservice, you MUST add your IP:userID to the parameter 'test.send-audio-to-ip' in /opt/cc/microservices/asr/features.conf on the ASR dev server. In addition, you must
 make sure both 'test.test-mode-enabled' and 'test.enable-audio-to-ip' are set to 'true' in the same file.

The port 55555 is hard-coded here, but you will have to switch out your own VPN IP below.

 More on capturing audio to a file: https://www.codejava.net/coding/capture-and-record-sound-into-wav-file-with-java-sound-api
*/

public class UDPServer {

    public static void main(String[] args) throws Exception {

        System.out.println("Running audio testing...");


        // This block is for the wav file that will be written
        File wavFile = new File("/home/bwagenseller/Downloads/RecordAudio.wav"); // path of the wav file
        AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE; // format of audio file
        PipedOutputStream filePipedOut=new PipedOutputStream();
        PipedInputStream filePipedIn; // line
        AudioInputStream fileAudioInputStream = null; // ais

        //create the actor system and the materializer
        final ActorSystem system = ActorSystem.create("asr-audio-testing");
        final ActorMaterializer materializer = ActorMaterializer.create(system);
        int pipeSize = 524288;//default is 1k (1024), but set it to 1 megabyte (1048576) or 256k (262144). 512k (524288) seems to be OK too

        SourceDataLine speakers;
        PipedOutputStream pipedOut=new PipedOutputStream();
        PipedInputStream pipedIn;

        boolean soundToSpeakers = true;
        boolean captureToFile = true;
        String IP = "10.248.128.204";
        //String IP = "127.0.0.1";
        int port = 55555;

        // This is the format that works with Asterisk
        AudioFormat format = new AudioFormat(8000, 16, 1, true, false);
        //AudioFormat format = new AudioFormat(16000, 16, 1, true, false); //use for mic testing ONLY

        final InetSocketAddress socket = new InetSocketAddress(IP, port);

        // set up the file stream
        if (captureToFile) {
            filePipedIn = new PipedInputStream(filePipedOut, pipeSize);
            fileAudioInputStream = new AudioInputStream(filePipedIn, format, AudioSystem.NOT_SPECIFIED);
        }

        if (soundToSpeakers) {
            //setup the speaker object
            pipedIn = new PipedInputStream(pipedOut,pipeSize);

            //audioInputStream=new AudioInputStream(pipedIn, format, AudioSystem.NOT_SPECIFIED);
            DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
            speakers = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
            speakers.open(format);
            speakers.start();
        } else {
            pipedIn = null;
            speakers = null;
        }

        //create a Udp.bindFlow flow that ALSO acts as a source. This takes the socket we are using (which is tied to the port)
        final Flow<Datagram, Datagram,CompletionStage<InetSocketAddress>> bindFlow = Udp.bindFlow(socket,system);  //returns the UDP flow

        //We actually need the ByteString of the Datagram and not the Datagram itself - so use a flow to extract the ByteString via getData
        final Flow<Datagram, ByteString, NotUsed> convertToByteString = Flow.of(Datagram.class).map(Datagram::getData);

        //we still need a source, technically, but it must be muted (as the UDP flow will act as a source)
        final Source ignoresource = Source.maybe(); //mute the source since its not needed to run our flow


        //final Sink<ByteString, InputStream> toStream = StreamConverters.asInputStream(Duration.ofMillis(3000));//Duration.ofSeconds(15)
        final Sink<ByteString, InputStream> toStream = StreamConverters.asInputStream(Duration.ofMillis(3000));//Duration.ofSeconds(15)

        //create a sink for the outlet of UDP flow; we are sending the data to the speaker
        final Sink toSpeaker = Sink.foreach(dataByteString -> {
            //just print something to show when we receive data

            byte[] data;
            byte[] rawData = ((ByteString)dataByteString).toArray();
            //System.out.println("Raw Data received: " + ((ByteString)dataByteString).toArray());

            // we do not have to do anything like ripping rtp headers off or decoding from ulaw to pcmu - we let the asr microservice do this and then it sends the raw data
            // to us, so we do not have to do any postprocessing of the data
            data = rawData;

            if (soundToSpeakers) speakers.write(data, 0, data.length);

            byte[] trashBytes = new byte[pipeSize];
            //System.out.println("Space left (pre): "+ pipedIn.available() + " length: " + data.length);
            if ((pipedIn.available() + data.length) > pipeSize) {
                System.out.println("DUMPING!");

                System.out.println("Space left (pre): "+ pipedIn.available() + " length: " + data.length);
                int trash = pipedIn.read(trashBytes);
                //pipedOut.flush();

                System.out.println("Space left (post): "+ pipedIn.available() + " length: " + data.length);

            }

            pipedOut.write(data);

            // also write to the pipedOutputStream for the file
            if (captureToFile) filePipedOut.write(data);
            //System.out.println("Space left (post): " + pipedIn.available());
        });


        if (soundToSpeakers) {
            final RunnableGraph<NotUsed> runnable = ignoresource.via(bindFlow).via(convertToByteString).to(toSpeaker);
            runnable.run(materializer);
        } else {

            final RunnableGraph<CompletableFuture<InputStream>> runnable = ignoresource.via(bindFlow).via(convertToByteString).to(toStream);
            CompletableFuture<InputStream> future = runnable.run(materializer);
            InputStream is = future.get();
            AudioInputStream audio = new AudioInputStream((TargetDataLine) is);

        }

        System.out.println("Akka stream setup complete - ready to accept stream...");

        // This sets up the write to the file. It needs to be done here, as it stalled elsewhere (this may block?)
        if (captureToFile) AudioSystem.write(fileAudioInputStream, fileType, wavFile);
    }

}