package com.clearcaptions.audio_test.stream;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.*;
import akka.stream.alpakka.udp.Datagram;
import akka.stream.alpakka.udp.javadsl.Udp;
import akka.stream.javadsl.*;
import akka.util.ByteString;
import com.clearcaptions.audio_test.internal_msgs.ActorSinkMsgs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * This class acts as _the_ stream that will handle the incoming UDP stream from Asterisk (or the microphone, if launching from a test).
 *
 * This object will bind to an IP and port, then will wait for incoming UDP data. It will change the data from the stream into a ByteString object, which it will send to an arbitrary
 * number of ActorRef sinks (a speaker object, vendor actors which will send the stream to a vendor, etc).
 *
 * This class should only be instantiated _once per caption session_, as there should only be one entity responsible for receiving the UDP stream - otherwise, there _will_ be problems with binding to the port.
 * It is intended to be instantiated in the ASRCaptionSessionActor.
 *
 * Some helpful links:
 * https://doc.akka.io/docs/alpakka/current/udp.html#
 * https://doc.akka.io/docs/akka/current/stream/stream-graphs.html#constructing-graphs
 * https://doc.akka.io/docs/akka/current/stream/operators/Sink/actorRefWithBackpressure.html
 */
public class UDPStream {

    private static final Logger logger = LoggerFactory.getLogger(UDPStream.class);

    // Akka Streams need access to both the actor system and a materializer in order to function.
    private Materializer materializer;
    private ActorSystem actorSystem;

    // We _must_ have an IP and port to bind to in order to accept incoming UDP packets. It should be noted that _nothing_ else shoul dbe using the port - not even another caption session.
    // The IP should be the IP of this node (I highly suggest using akka.remote.artery.canonical.hostname from application.conf)
    private String boundIP;
    private int port;

    //teh killswitch needed for stopping the stream
    private UniqueKillSwitch killSwitch;

    /*
     ************************************************************************************************************************************
     ************************************************************************************************************************************
     ************************************************************CONSTRUCTORS************************************************************
     ************************************************************************************************************************************
     ************************************************************************************************************************************
     */
    public UDPStream(ActorSystem actorSystem, Materializer materializer, String boundIP, int port) {
        this.actorSystem = actorSystem;
        //this.materializer = Materializer.createMaterializer(actorSystem);
        this.materializer = materializer;
        this.boundIP = boundIP;
        this.port = port;
    }

    /**
     * Performs the shutdown actions of the class.
     *
     */
    public void shutdown() {

        // shutdown the stream
        killSwitch.shutdown();

        //shut down the materializer
        //this.materializer.shutdown();
    }

    /**
     *
     * This starts a simple UDP linear stream with an arbitrary number of actor sinks (at least one is needed). Externally, this should be identical to
     * startUDPStreamWithMultipleActorSinksDSL, but you should use this method unless there is an absolutely credible reason to use startUDPStreamWithMultipleActorSinksDSL.
     *
     * @param actorSinkList A listing of all actorRefs that represent actors that will act as a sink in the stream. Note these actors _MUST_ accept the following messages:
     *
     *     ByteString.class (which is the actual payload from the stream)
     *     ActorSinkMsgs.StreamInitialized.class
     *     ActorSinkMsgs.StreamCompleted.class
     *     ActorSinkMsgs.StreamFailure.class
     *
     *   In addition, the actor MUST respond to every message this sinks sends with: ActorSinkMsgs.Ack.ACKNOWLEDGE
     */
    public void startUDPStreamWithMultipleActorSinks(List<ActorRef> actorSinkList)  {

        Sink<ByteString, NotUsed> tempSink;
        final List<Sink<ByteString, NotUsed>> list = new ArrayList<>();
        String sinkListing = "";

        // Create the socket. the IP and port combination can only be bound ONCE until destroyed - the same IP / port cannot be active in multiple streams!
        final InetSocketAddress socket = new InetSocketAddress(this.boundIP, this.port);

        //create a Udp.bindFlow flow that ALSO acts as a pseudo source. This takes the socket we are using (which is tied to the port)
        final Flow<Datagram, Datagram, CompletionStage<InetSocketAddress>> bindFlow = Udp.bindFlow(socket,this.actorSystem);  //returns the UDP flow

        //we still need a source, technically, but it must be muted (as the UDP flow will act as a source)
        //mute the source since its not needed to run our flow
        final Source ignoresource = Source.maybe();

        //We actually need the ByteString of the Datagram and not the Datagram itself - so use a flow to extract the ByteString via getData
        final Flow<Datagram, ByteString, NotUsed> convertToByteString = Flow.of(Datagram.class).map(Datagram::getData);

        // this is an example of how to tie-in a flow into a home-grown method; this method once existed in this actor
        //final Flow<ByteString, ByteString, NotUsed> toneFlow = Flow.of(ByteString.class).map(msg -> this.someMethodInThisClass(msg, captionSessionActorRef));

        //create a list of sinks for the outlet of UDP flow; we are sending the data to each actorRef. Note these MUST be wired up with the listed messages!
        for (ActorRef someActor: actorSinkList) {
            tempSink = Sink.actorRefWithBackpressure(
                    someActor,
                    new ActorSinkMsgs.StreamInitialized(),
                    ActorSinkMsgs.Ack.ACKNOWLEDGE,
                    new ActorSinkMsgs.StreamCompleted(),
                    ex -> new ActorSinkMsgs.StreamFailure(ex));
            list.add(tempSink);

            // save the actor name to our list, which we will print to the log
            sinkListing = sinkListing + ((sinkListing.equals(""))?"":", ") + someActor.path().name();
        }

        //Build the graph, to the point where we need the sinks. Note that this is a 'Source' although it contains two flows
        Source toBeCompletedLinearStream = ignoresource.via(bindFlow).via(convertToByteString);

        //add the kill switch
        toBeCompletedLinearStream = toBeCompletedLinearStream.viaMat(KillSwitches.single(), Keep.right());

        // add all sinks but the last one; this is because in order to add multiple sinks we must use the 'alsoTo' method, but they need to come BEFORE the final 'to' that
        // hooks up the last remaining sink (there _always_ needs to be exactly one 'to', and it needs to come last)
        // if we did not care about the kill switch, we would just use '.alsoTo' here instead of '.alsoToMat'; in addition, since we do not cate about the materialized value of the stream,
        // just keep right (the kill switch))
        for (int i = 0;i<=(list.size() - 2);i++) toBeCompletedLinearStream = toBeCompletedLinearStream.alsoToMat(list.get(i), Keep.left());

        // Add the last sink and create a runnable graph
        // if we did not care about the kill switch, we would just use '.to' here instead of '.toMat'; in addition, since we do not cate about the materialized value of the stream, just keep
        // right (the kill switch)); finally, if we did not care about the kill switch, we could just use 'RunnableGraph<NotUsed>' here
        final RunnableGraph<UniqueKillSwitch> runnable = toBeCompletedLinearStream.toMat(list.get((list.size()-1)), Keep.left());

        logger.debug("Starting a (linear) UDP stream; stream sinks are [{}].", sinkListing);

        // run the graph, but ALSO save the accessible killswitch
        killSwitch = runnable.run(this.materializer);
    }

    /**
     *
     * This starts a Graph DSL stream with an arbitrary number of actor sinks (at least one is needed). Externally, this should be identical to startUDPStreamWithMultipleActorSinks,
     * but you should use startUDPStreamWithMultipleActorSinks unless there is an absolutely credible reason to use this one.
     *
     * @param actorSinkList A listing of all actorRefs that represent actors that will act as a sink in the stream. Note these actors _MUST_ accept the following messages:
     *
     *     ByteString.class (which is the actual payload from the stream)
     *     ActorSinkMsgs.StreamInitialized.class
     *     ActorSinkMsgs.StreamCompleted.class
     *     ActorSinkMsgs.StreamFailure.class
     *
     *   In addition, the actor MUST respond to every message this sinks sends with: ActorSinkMsgs.Ack.ACKNOWLEDGE
     */
    public void startUDPStreamWithMultipleActorSinksDSL(List<ActorRef> actorSinkList)  {

        //todo: THIS WORKS, BUT I COULD NOT GET THE KILLSWITCH TO WORK - SO THERE WILL BE PORT ISSUES

        final List<Sink<ByteString, NotUsed>> list = new ArrayList<>();
        Sink<ByteString, NotUsed> tempSink;
        String sinkListing = "";

        // Create the socket. the IP and port combination can only be bound ONCE until destroyed - the same IP / port cannot be active in multiple streams!
        final InetSocketAddress socket = new InetSocketAddress(this.boundIP, this.port);

        //create a Udp.bindFlow flow that ALSO acts as a pseudo source. This takes the socket we are using (which is tied to the port)
        final Flow<Datagram, Datagram, CompletionStage<InetSocketAddress>> bindFlow = Udp.bindFlow(socket,this.actorSystem);  //returns the UDP flow

        //we still need a source, technically, but it must be muted (as the UDP flow will act as a source)
        //mute the source since its not needed to run our flow
        final Source<Datagram, CompletableFuture<Optional<Datagram>>> ignoresource = Source.<Datagram>maybe();

        //We actually need the ByteString of the Datagram and not the Datagram itself - so use a flow to extract the ByteString via getData
        final Flow<Datagram, ByteString, NotUsed> convertToByteString = Flow.of(Datagram.class).map(Datagram::getData);

        //create a list of sinks for the outlet of UDP flow; we are sending the data to each actorRef. Note these MUST be wired up with the listed messages!
        for (ActorRef someActor: actorSinkList) {
            tempSink = Sink.actorRefWithBackpressure(
                    someActor,
                    new ActorSinkMsgs.StreamInitialized(),
                    ActorSinkMsgs.Ack.ACKNOWLEDGE,
                    new ActorSinkMsgs.StreamCompleted(),
                    ex -> new ActorSinkMsgs.StreamFailure(ex));
            list.add(tempSink);

            // save the actor name to our list, which we will print to the log
            sinkListing = sinkListing + ((sinkListing.equals(""))?"":", ") + someActor.path().name();
        }

        // wire up the DSL graph. The builder can be a bit tricky, but remember:
        //   * The 'list' is stored as the 'outs' object in the lambda
        //   * a broadcast (UniformFanOutShape) object is needed to have multiple sinks - it takes one parameter which is the number of sinks we will use
        //   * The source must be defined before the builder is engaged.
        //   * The builder must include the source in builder.from(source)
        //   * All flows can be added with .via() in the order they are needed
        //   * Before we finalize the ClosedShape, we need to make sure EVERY sink is connected from the broadcast with a 'builder.from(bcast).to(sink)' (where sink changes in a for loop)
        final RunnableGraph<List<NotUsed>> theGraph =
                RunnableGraph.fromGraph(
                        GraphDSL.create(
                                list,
                                ((builder, outs) -> {
                                    final UniformFanOutShape<ByteString, ByteString> bcast = builder.add(Broadcast.create(outs.size()));

                                    final Outlet<Datagram> source = builder.add(ignoresource).out();
                                    builder.from(source)
                                            .via(builder.add(bindFlow))
                                            .via(builder.add(convertToByteString))
                                            //.via(builder.add(KillSwitches.single()))
                                            .viaFanOut(bcast);

                                    for (SinkShape<ByteString> sink : outs) builder.from(bcast).to(sink);

                                    return ClosedShape.getInstance();
                                })));

        logger.debug("Starting a (graph) UDP stream; stream sinks are [{}].", sinkListing);

        // Run the graph!
        List<NotUsed> result = theGraph.run(this.materializer);
    }
}
