package com.clearcaptions.audio_test.config;

import com.google.common.base.Strings;
import java.util.Map;
import org.slf4j.Logger;

/**
 * This class is used to check basic properties of values in a map. Most of the methods work by checking the map for the given key and then making sure the value can be converted to the desired value;
 * if it cannot be - or if the key is missing - the default given value is used.
 */
public class CheckMapValues {

    /**
     * (For DOUBLE Values) This simply reads a config object and gets an alleged double value via the given key; the key/value pair is then placed on the provided map.
     * This is usually used in conjunction with the method getSupervisorConfigValues().
     *
     * @param logger The logger object. This is required in the event there are any issues with the data.
     * @param proposedConfig a Map object that is pulled from a response from AcdConfigActor.
     * @param key The key for the returned map (usually an entry in proposedConfig).
     * @param defaultValue The default double value, if the value is not found.
     * @return The given double value that was the result of investigating the map object.
     */
    public static double getConfigValue(Logger logger, Map<String,String> proposedConfig, String key, double defaultValue) {

        double retVal;

        if(proposedConfig.containsKey(key)) {
            try {
                retVal = Double.parseDouble(proposedConfig.get(key));
            } catch (NumberFormatException nfe) {
                logger.error("getConfigValue (Double) :: config value [{}] was not a double. Using default of [{}].", key, defaultValue);
                retVal = defaultValue;
            }
        } else {
            logger.warn("getConfigValue (Double) :: config value [{}] was not present. Using default of [{}].", key, defaultValue);
            retVal = defaultValue;
        }

        return retVal;
    }

    /**
     * (For INTEGER Values) This simply reads a config object and gets an alleged integer value via the given key; the key/value pair is then placed on the provided map.
     * This is usually used in conjunction with the method getSupervisorConfigValues().
     *
     * @param logger The logger object. This is required in the event there are any issues with the data.
     * @param proposedConfig a Map object that is pulled from a response from AcdConfigActor.
     * @param key The key for the returned map (usually an entry in proposedConfig).
     * @param defaultValue The default integer value, if the value is not found.
     * @return The given integer value that was the result of investigating the map object.
     */
    public static int getConfigValue(Logger logger, Map<String,String> proposedConfig, String key, int defaultValue) {

        int retVal;

        if(proposedConfig.containsKey(key)) {
            try {
                retVal = Integer.parseInt(proposedConfig.get(key));
            } catch (NumberFormatException nfe) {
                logger.error("getConfigValue (Integer) :: config value [{}] was not an integer. Using default of [{}].", key, defaultValue);
                retVal = defaultValue;
            }
        } else {
            logger.warn("getConfigValue (Integer) :: config value [{}] was not present. Using default of [{}].", key, defaultValue);
            retVal = defaultValue;
        }

        return retVal;
    }

    /**
     * (For LONG Values) This simply reads a config object and gets an alleged long value via the given key; the key/value pair is then placed on the provided map.
     * This is usually used in conjunction with the method getSupervisorConfigValues().
     *
     * @param logger The logger object. This is required in the event there are any issues with the data.
     * @param proposedConfig a Map object that is pulled from a response from AcdConfigActor.
     * @param key The key for the returned map (usually an entry in proposedConfig).
     * @param defaultValue The default long value, if the value is not found.
     * @return The given long value that was the result of investigating the map object.
     */
    public static long getConfigValue(Logger logger, Map<String,String> proposedConfig, String key, long defaultValue) {

        long retVal;

        if(proposedConfig.containsKey(key)) {
            try {
                retVal = Long.parseLong(proposedConfig.get(key));
            } catch (NumberFormatException nfe) {
                logger.error("getConfigValue (Long) :: config value [{}] was not a long. Using default of [{}].", key, defaultValue);
                retVal = defaultValue;
            }
        } else {
            logger.warn("getConfigValue (Long) :: config value [{}] was not present. Using default of [{}].", key, defaultValue);
            retVal = defaultValue;
        }

        return retVal;
    }


    /**
     * (For BOOLEAN Values) This simply reads a config object and gets an alleged boolean value via the given key; the key/value pair is then placed on the provided map.
     * This is usually used in conjunction with the method getSupervisorConfigValues().
     *
     * @param logger The logger object. This is required in the event there are any issues with the data.
     * @param proposedConfig a Map object that is pulled from a response from AcdConfigActor.
     * @param key The key for the returned map (usually an entry in proposedConfig).
     * @param defaultValue The default boolean value, if the value is not found.
     * @return The given boolean value that was the result of investigating the map object.
     */
    public static boolean getConfigValue(Logger logger, Map<String,String> proposedConfig, String key, boolean defaultValue) {

        boolean retVal;

        if(proposedConfig.containsKey(key)) {
            try {
                retVal = Boolean.parseBoolean(proposedConfig.get(key));
            } catch (Exception e) {
                logger.error("getConfigValue (Boolean) :: config value [{}] was not a boolean. Using default of [{}].", key, defaultValue);
                retVal = defaultValue;
            }
        } else {
            logger.warn("getConfigValue (Boolean) :: config value [{}] was not present. Using default of [{}].", key, defaultValue);
            retVal = defaultValue;
        }

        return retVal;
    }

    /**
     * (For STRING Values) This simply reads a config object and gets an alleged String value via the given key; the key/value pair is then placed on the provided map.
     * This is usually used in conjunction with the method getSupervisorConfigValues().
     *
     * @param logger The logger object. This is required in the event there are any issues with the data.
     * @param proposedConfig a Map object that is pulled from a response from AcdConfigActor.
     * @param key The key for the returned map (usually an entry in proposedConfig).
     * @param defaultValue The default String value, if the value is not found.
     * @return The given String value that was the result of investigating the map object.
     */
    public static String getConfigValue(Logger logger, Map<String,String> proposedConfig, String key, String defaultValue) {

        String retVal;

        if(proposedConfig.containsKey(key)) {
            if(!(Strings.isNullOrEmpty(proposedConfig.get(key)))) {
                retVal = proposedConfig.get(key);
            } else {
                logger.error("getConfigValue (String) :: config value [{}] was null or empty. Using default of [{}].", key, defaultValue);
                retVal = defaultValue;
            }
        } else {
            logger.warn("getConfigValue (String) :: config value [{}] was not present. Using default of [{}].", key, defaultValue);
            retVal = defaultValue;
        }

        return retVal;
    }

    /**
     * Check to see if a given string is numeric; if so, a true is returned, false otherwise.
     *
     * @param strNum The string that may possibly contain a number.
     * @return If the given string was indeed numberic; a boolean true; false otherwise.
     */
    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            //check for a double, as this will also work for integers as well
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}