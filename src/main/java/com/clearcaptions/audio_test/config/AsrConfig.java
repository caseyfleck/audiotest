package com.clearcaptions.audio_test.config;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.regions.Region;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This class is a bastardized version of AsrConfigActor and AsrConfig from the ASR microservice. It is intended to be loaded once and only once (so this does not watch
 * features.conf for changes - if you make changes, you must re-start the app).
 */

public class AsrConfig {

    private static final Logger logger = LoggerFactory.getLogger(AsrConfig.class);

    private static final String FEATURE_CONFIG_FILE=System.getProperty("user.dir")+"/features.conf";
    private final Properties configuration=new Properties();
    private Path configPath;

    private final double IBM_END_OF_PHRASE_SIELNCE_TIME = .6;
    private final String AWS_REGION = "us-west-2";
    private final String AWS_CREDENTIAL_FILE = "/root/.credentials/aws/credentials";
    private final String IBM_CREDENTIAL_FILE = "/root/.credentials/ibm/ibm-credentials.env";
    private final long GOOGLE_CLIENT_STREAM_BLOCKING_TIME = 2000;
    private final long GOOGLE_RECONNECT_TIME = 180000;
    private final String GOOGLE_TRANSCRIPTION_MODEL = "phone_call";
    private final int UDP_STREAM_TIMEOUT = 15;
    private final String MY_IP = "127.0.0.1";
    private final int MY_PORT = 55555;
    private final String MY_AUDIO_FILE_DIRECTORY = "~/Downloads";
    private final String MY_AUDIO_FILE_NAME = "AudioRecording";
    private final int SILENCE_TIMEOUT = 18000;

    // This is simply stored as a CSV as I do not know how well the GSON converter wil play with it
    private String approvedVendorCSVString;

    private int minRecreationSeconds;
    private int maxRecreationSeconds;
    private double recreationIntervalNoise;
    private int retriesInXSeconds;
    private int secondsInRetries;
    private int audioFormatSampleRate;
    private int audioFormatSampleSizeInBits;
    private int audioFormatChannels;
    private boolean audioFormatPcmSigned;
    private boolean audioFormatBigEndian;
    private int audioFormatInputStreamPipeSize;
    private double ibmEndOfPhraseSilenceTime;
    private String ibmCredentialFile;
    private String awsRegion;
    private String awsCredentialFile;
    private long googleClientStreamBlockingTime;
    private long googleReconnectTime;
    private String googleTranscriptionModel;
    private int udpStreamTimeout;
    private String myIP;
    private int myPort;
    private String myAudioFileDirectory;
    private String myAudioFileName;
    private int silenceTimeout;

    // A constructor with a parameter is needed - otherwise the copy will also re-load the file
    public AsrConfig(boolean loadFromFileOnStart) {
        if (loadFromFileOnStart) {
            this.configPath = Paths.get(FEATURE_CONFIG_FILE);
            try {
                loadConfiguration();
            } catch (IOException e) {
                logger.error("Failed to load configuration!", e);
            }
        }
    }

    public AsrConfig() {
        // do nothing
        this.configPath = Paths.get(FEATURE_CONFIG_FILE);
    }

    /*
     *************************************************************************************************************************************
     *************************************************************************************************************************************
     ************************************************************Class Methods************************************************************
     *************************************************************************************************************************************
     *************************************************************************************************************************************
     */

    /**
     * We have some user-defined variables that the AcdConfigActor pulls in that we will use in the statistic actor family. Since many checks need to be performed
     * when getting variables, this is a centralized place that takes care of this.
     *
     * @param proposedConfig a Map object that is pulled from a response from AcdConfigActor.
     */
    public void load(Map<String,String> proposedConfig) {

        minRecreationSeconds = CheckMapValues.getConfigValue(logger, proposedConfig, "akka.backoffsupervisor.min-recreation-seconds", 3);
        maxRecreationSeconds = CheckMapValues.getConfigValue(logger, proposedConfig, "akka.backoffsupervisor.max-recreation-seconds", 30);
        recreationIntervalNoise = CheckMapValues.getConfigValue(logger, proposedConfig, "akka.backoffsupervisor.recreation-interval-noise", .2);
        retriesInXSeconds = CheckMapValues.getConfigValue(logger, proposedConfig, "akka.decider-builder.retries-in-x-seconds", 5);
        secondsInRetries = CheckMapValues.getConfigValue(logger, proposedConfig, "akka.decider-builder.seconds-in-retries", 30);
        audioFormatSampleRate = CheckMapValues.getConfigValue(logger, proposedConfig, "audio-format.sample-rate", 8000);
        audioFormatSampleSizeInBits = CheckMapValues.getConfigValue(logger, proposedConfig, "audio-format.sample-size-in-bits", 16);
        audioFormatChannels = CheckMapValues.getConfigValue(logger, proposedConfig, "audio-format.channels", 1);
        audioFormatPcmSigned = CheckMapValues.getConfigValue(logger, proposedConfig, "audio-format.pcm-signed", true);
        audioFormatBigEndian = CheckMapValues.getConfigValue(logger, proposedConfig, "audio-format.big-endian", false);
        audioFormatInputStreamPipeSize = CheckMapValues.getConfigValue(logger, proposedConfig, "audio-format.inputStreamPipeSize", 131072);
        approvedVendorCSVString = CheckMapValues.getConfigValue(logger, proposedConfig, "asr-vendors.approved-list", "IBM,AWS");
        silenceTimeout = CheckMapValues.getConfigValue(logger, proposedConfig, "asr-vendors.silence-timeout", SILENCE_TIMEOUT);
        ibmEndOfPhraseSilenceTime = CheckMapValues.getConfigValue(logger, proposedConfig, "ibm.end-of-phrase-silence-time", IBM_END_OF_PHRASE_SIELNCE_TIME);
        googleClientStreamBlockingTime = CheckMapValues.getConfigValue(logger, proposedConfig, "google.client-stream-blocking-time", GOOGLE_CLIENT_STREAM_BLOCKING_TIME);
        googleReconnectTime = CheckMapValues.getConfigValue(logger, proposedConfig, "google.reconnect-time", GOOGLE_RECONNECT_TIME);
        googleTranscriptionModel = CheckMapValues.getConfigValue(logger, proposedConfig, "google.transcription-model", GOOGLE_TRANSCRIPTION_MODEL);
        awsRegion = CheckMapValues.getConfigValue(logger, proposedConfig, "aws.region", AWS_REGION);
        awsCredentialFile = CheckMapValues.getConfigValue(logger, proposedConfig, "aws.credential-file", AWS_CREDENTIAL_FILE);
        ibmCredentialFile = CheckMapValues.getConfigValue(logger, proposedConfig, "ibm.credential-file", IBM_CREDENTIAL_FILE);
        udpStreamTimeout = CheckMapValues.getConfigValue(logger, proposedConfig, "caption-session.udp-stream-timeout", UDP_STREAM_TIMEOUT);
        myIP = CheckMapValues.getConfigValue(logger, proposedConfig, "my.ip", MY_IP);
        myPort = CheckMapValues.getConfigValue(logger, proposedConfig, "my.port", MY_PORT);
        myAudioFileDirectory = CheckMapValues.getConfigValue(logger, proposedConfig, "my.audio-file-directory", MY_AUDIO_FILE_DIRECTORY);
        myAudioFileName = CheckMapValues.getConfigValue(logger, proposedConfig, "my.audio-file-name", MY_AUDIO_FILE_NAME);

    }


    /**
     * This checks to make sure the AWS region is right
     */
    private void checkAwsRegion() {
        List<String> realAwsRegionList = new ArrayList<>();

        String allRegions = "";
        for(Region oneRegion : Region.regions()) {
            String strRegion = oneRegion.toString();
            realAwsRegionList.add(strRegion);
            allRegions = allRegions + ((allRegions.equals("")) ? "" : ", ") + strRegion;
        }

        if( realAwsRegionList.contains(awsRegion) == false) {

            logger.error("'aws.region' [{}] must be one of the following: [{}]. Defaulting to [{}].", awsRegion, allRegions, AWS_REGION);
            awsRegion = AWS_REGION;
        }
    }

    public void checkAwsCredentialFile() {
        if(!doesFileExist(this.awsCredentialFile)) {
            logger.error("AWS credential file [{}] does not exist - attempting default of [{}].", this.awsCredentialFile, AWS_CREDENTIAL_FILE);
            if(!doesFileExist(AWS_CREDENTIAL_FILE)) {
                logger.error("Default AWS credential file [{}] does not exist - since there is no credential file, AWS Transcribe will be inoperable. If AWS exists in the approved list of vendors it will be removed..", AWS_CREDENTIAL_FILE);

                // remove by simply replacing it in the CSV string - it will probably leave a double ",," so get rid of that too (also get rid of spaces while we are at it)
                this.approvedVendorCSVString = this.approvedVendorCSVString.replace(" ","").replace("AWS","").replace(",,",",");
            } else { this.awsCredentialFile = AWS_CREDENTIAL_FILE; }
        }
    }

    public void checkIbmCredentialFile() {
        if(!doesFileExist(this.ibmCredentialFile)) {
            logger.error("IBM credential file [{}] does not exist - attempting default of [{}].", this.ibmCredentialFile, IBM_CREDENTIAL_FILE);
            if(!doesFileExist(IBM_CREDENTIAL_FILE)) {
                logger.error("Default IBM credential file [{}] does not exist - since there is no credential file, IBM Watson will be inoperable. If IBM exists in the approved list of vendors it will be removed..", IBM_CREDENTIAL_FILE);

                // remove by simply replacing it in the CSV string - it will probably leave a double ",," so get rid of that too (also get rid of spaces while we are at it)
                this.approvedVendorCSVString = this.approvedVendorCSVString.replace(" ","").replace("IBM","").replace(",,",",");
            } else { this.ibmCredentialFile = IBM_CREDENTIAL_FILE; }
        }
    }

    /**
     * Check if a file exists and is a file.
     */
    private boolean doesFileExist(String filePathAndFileName) {
        File f = new File(filePathAndFileName);
        return (f.exists() && !f.isDirectory());
    }



    /**
     * Override the toString() method; this now returns a JSON string representation of the object.
     *
     * @return A JSON string of this object.
     */
    public String toString() { return new Gson().toJson(this); }

    /*
     *******************************************************************************************************************************
     *******************************************************************************************************************************
     ************************************************************Getters************************************************************
     *******************************************************************************************************************************
     *******************************************************************************************************************************
     */

    public double getRecreationIntervalNoise() { return this.recreationIntervalNoise; }
    public int getMinRecreationSeconds() { return this.minRecreationSeconds; }
    public int getMaxRecreationSeconds() { return this.maxRecreationSeconds; }
    public int getRetriesInXSeconds() { return this.retriesInXSeconds; }
    public int getSecondsInRetries() { return this.secondsInRetries; }
    public int getAudioFormatSampleRate() { return this.audioFormatSampleRate; }
    public int getAudioFormatSampleSizeInBits() { return this.audioFormatSampleSizeInBits; }
    public int getAudioFormatChannels() { return this.audioFormatChannels; }
    public int getAudioFormatInputStreamPipeSize() { return this.audioFormatInputStreamPipeSize; }
    public boolean getAudioFormatPcmSigned() { return this.audioFormatPcmSigned; }
    public boolean getAudioFormatBigEndian() { return this.audioFormatBigEndian; }
    public String getApprovedVendorCSVString() { return this.approvedVendorCSVString; }
    public long getGoogleClientStreamBlockingTime() { return this.googleClientStreamBlockingTime; }
    public long getGoogleReconnectTime() { return this.googleReconnectTime; }
    public String getGoogleTranscriptionModel() { return this.googleTranscriptionModel; }
    public double getIbmEndOfPhraseSilenceTime() { return this.ibmEndOfPhraseSilenceTime; }
    public String getAwsRegion() { return this.awsRegion; }
    public String getAwsCredentialFile() { return this.awsCredentialFile; }
    public String getIbmCredentialFile() { return this.ibmCredentialFile; }
    public int getUdpStreamTimeout() { return this.udpStreamTimeout; }
    public String getMyIP() { return this.myIP; }
    public int getMyPort() { return this.myPort; }
    public String getMyAudioFileDirectory() { return this.myAudioFileDirectory; }
    public String getMyAudioFileName() { return this.myAudioFileName; }
    public int getSilenceTimeout() { return this.silenceTimeout; }

    /**
     * approvedVendorCSVString is a CSV string as I do not know how well the GSON converter will play with it; this getter method is included to quickly convert to a list.
     *
     * @return approvedVendorCSVString as an ArrayList(String)
     */
    public List<String> getApprovedVendorsAsList() {
        List<String> allApprovedVendors = new ArrayList<>();

        // add all of the CSV values to tempAllVendors - make sure that we do NOT add "" though, if this is there the list is empty (i.e. there are no approved vendors)
        for (String oneVendor:approvedVendorCSVString.split(",")) {
            if (!oneVendor.equals("")) {
                allApprovedVendors.add(oneVendor.toUpperCase());
            }
        }

        return allApprovedVendors;
    }


    /*
     *******************************************************************************************************************************
     *******************************************************************************************************************************
     ************************************************************Setters************************************************************
     *******************************************************************************************************************************
     *******************************************************************************************************************************
     */

    public void setRecreationIntervalNoise(double recreationIntervalNoise) { this.recreationIntervalNoise = recreationIntervalNoise; }
    public void setMinRecreationSeconds(int minRecreationSeconds) { this.minRecreationSeconds = minRecreationSeconds; }
    public void setMaxRecreationSeconds(int maxRecreationSeconds) { this.maxRecreationSeconds = maxRecreationSeconds; }
    public void setRetriesInXSeconds(int retriesInXSeconds) { this.retriesInXSeconds = retriesInXSeconds; }
    public void setSecondsInRetries(int secondsInRetries) { this.secondsInRetries = secondsInRetries; }
    public void setAudioFormatSampleRate(int audioFormatSampleRate) { this.audioFormatSampleRate = audioFormatSampleRate; }
    public void setAudioFormatSampleSizeInBits(int audioFormatSampleSizeInBits) { this.audioFormatSampleSizeInBits = audioFormatSampleSizeInBits; }
    public void setAudioFormatChannels(int audioFormatChannels) { this.audioFormatChannels = audioFormatChannels; }
    public void setAudioFormatInputStreamPipeSize(int audioFormatInputStreamPipeSize) { this.audioFormatInputStreamPipeSize= audioFormatInputStreamPipeSize; }
    public void setAudioFormatPcmSigned(boolean audioFormatPcmSigned) { this.audioFormatPcmSigned = audioFormatPcmSigned; }
    public void setAudioFormatBigEndian(boolean audioFormatBigEndian) { this.audioFormatBigEndian = audioFormatBigEndian; }
    public void setGoogleClientStreamBlockingTime(long googleClientStreamBlockingTime) { this.googleClientStreamBlockingTime = googleClientStreamBlockingTime; }
    public void setGoogleReconnectTime(long googleReconnectTime) { this.googleReconnectTime = googleReconnectTime; }
    public void setGoogleTranscriptionModel(String googleTranscriptionModel) { this.googleTranscriptionModel = googleTranscriptionModel; }
    public void setIbmEndOfPhraseSilenceTime(double ibmEndOfPhraseSilenceTime) { this.ibmEndOfPhraseSilenceTime = ibmEndOfPhraseSilenceTime; }
    public void setUdpStreamTimeout(int udpStreamTimeout) { this.udpStreamTimeout = udpStreamTimeout; }
    public void setMyIP(String myIP) { this.myIP = myIP; }
    public void setMyPort(int myPort) { this.myPort = myPort; }
    public void setMyAudioFileDirectory(String myAudioFileDirectory) { this.myAudioFileDirectory = myAudioFileDirectory; }
    public void setMyAudioFileName(String myAudioFileName) { this.myAudioFileName = myAudioFileName; }
    public void setApprovedVendorCSVString(String approvedVendorCSVString) { this.approvedVendorCSVString = approvedVendorCSVString; }
    public void setSilenceTimeout(int silenceTimeout) { this.silenceTimeout = silenceTimeout; }

    public void setAwsRegion(String awsRegion) {
        this.awsRegion = awsRegion;
        checkAwsRegion();
    }
    public void setAwsCredentialFile(String awsCredentialFile) {
        this.awsCredentialFile = awsCredentialFile;
        checkAwsCredentialFile();
    }
    public void setIbmCredentialFile(String ibmCredentialFile) {
        this.ibmCredentialFile = ibmCredentialFile;
        checkIbmCredentialFile();
    }

    /*
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     *************************************************Methods that come from AsrConfigActor************************************************
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     */

    /**
     * This creates a deep copy of this StatisticConfig, which can act independently.
     *
     * @return a deep copy of this StatisticConfig.
     */
    public AsrConfig copy() {

        AsrConfig retVal = new AsrConfig();

        retVal.setMinRecreationSeconds(this.getMinRecreationSeconds());
        retVal.setMaxRecreationSeconds(this.getMaxRecreationSeconds());
        retVal.setRecreationIntervalNoise(this.getRecreationIntervalNoise());
        retVal.setRetriesInXSeconds(this.getRetriesInXSeconds());
        retVal.setSecondsInRetries(this.getSecondsInRetries());
        retVal.setAudioFormatSampleRate(this.getAudioFormatSampleRate());
        retVal.setAudioFormatSampleSizeInBits(this.getAudioFormatSampleSizeInBits());
        retVal.setAudioFormatChannels(this.getAudioFormatChannels());
        retVal.setAudioFormatPcmSigned(this.getAudioFormatPcmSigned());
        retVal.setAudioFormatBigEndian(this.getAudioFormatBigEndian());
        retVal.setAudioFormatInputStreamPipeSize(this.getAudioFormatInputStreamPipeSize());
        retVal.setGoogleClientStreamBlockingTime(this.getGoogleClientStreamBlockingTime());
        retVal.setGoogleReconnectTime(this.getGoogleReconnectTime());
        retVal.setGoogleTranscriptionModel(this.getGoogleTranscriptionModel());
        retVal.setIbmEndOfPhraseSilenceTime(this.getIbmEndOfPhraseSilenceTime());
        retVal.setAwsRegion(this.getAwsRegion());
        retVal.setAwsCredentialFile(this.getAwsCredentialFile());
        retVal.setIbmCredentialFile(this.getIbmCredentialFile());
        retVal.setUdpStreamTimeout(this.getUdpStreamTimeout());
        retVal.setMyPort(this.getMyPort());
        retVal.setMyIP(this.getMyIP());
        retVal.setMyAudioFileDirectory(this.getMyAudioFileDirectory());
        retVal.setMyAudioFileName(this.getMyAudioFileName());
        retVal.setApprovedVendorCSVString(this.getApprovedVendorCSVString());
        retVal.setSilenceTimeout(this.getSilenceTimeout());

        return retVal;
    }

    /**
     * The statistic actor family needs a few prefixes from AcdConfigActor; these are listed here and can be used in a request.
     *
     * @return An ArrayList of the prefixes the statistic actor family will use.
     */
    public Map<String, String> getSelectedContents() {

        // List the prefixes we want from the acd config actor
        List<String> requestedPrefixes = new ArrayList<>();
        requestedPrefixes.add("my");
        requestedPrefixes.add("akka.backoffsupervisor");
        requestedPrefixes.add("akka.decider-builder");
        requestedPrefixes.add("asr-vendors");
        requestedPrefixes.add("audio-format");
        requestedPrefixes.add("ibm");
        requestedPrefixes.add("aws");
        requestedPrefixes.add("google");
        requestedPrefixes.add("caption-session");
        Map<String, String> found = new HashMap<>();

        for (String prefix: requestedPrefixes) found.putAll(getEntriesForOnePrefix(prefix));

        return found;
    }

    /**
     * This internal method takes one prefix and outputs the list of entries.
     *
     * @param prefix The prefix of the setting group we want to return.
     * @return a Map<String, String> that contains the keys and values associated with the prefix.
     */
    private Map<String, String> getEntriesForOnePrefix(String prefix) {
        int prefixLen=prefix.length();

        // if prefix strip is enabled and the prefix is the same as the key then the key is a blank string
        Function<String, String> keyFunction = key -> key;

        Set<String> keys = new HashSet<>(configuration.stringPropertyNames());
        Map<String, String> onePrefixGrouping = keys.stream()
                .filter(key -> key.startsWith(prefix))
                .collect(Collectors.toMap(keyFunction,
                        key -> configuration.getProperty(key)));

        return onePrefixGrouping;
    }

    /**
     * Loads configuration from the file system.
     * @throws IOException If an IO error occurs.
     */
    protected void loadConfiguration() throws IOException {
        logger.debug("loading configuration from {} ...", configPath);
        File f=configPath.toFile();
        if (!f.exists()) {
            throw new FileNotFoundException("Feature configuration file "+configPath+" does not exist!");
        }
        configuration.clear();
        try (InputStream is=new FileInputStream(f)) {
            configuration.load(is);
        }
        logger.debug("configuration reloaded from {} with {} entries", configPath, configuration.size());

        load(getSelectedContents());
    }
}

