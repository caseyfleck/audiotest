package com.clearcaptions.audio_test.google_stt;

import akka.actor.AbstractActor;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.util.ByteString;
import com.clearcaptions.audio_test.config.AsrConfig;
import com.clearcaptions.audio_test.internal_msgs.ActorSinkMsgs;
import com.google.api.gax.rpc.ClientStream;
import com.google.api.gax.rpc.ResponseObserver;
import com.google.api.gax.rpc.StreamController;
//import com.google.cloud.speech.v1p1beta1.*; // this is google's beta, which includes language detection
import com.google.cloud.speech.v1.*;
import com.google.protobuf.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**

 */
public class GoogleSttActor extends AbstractActor {

    private static final Logger logger = LoggerFactory.getLogger(GoogleSttActor.class);

    // Since this actor will only last a few minutes, this config actor should not change
    private AsrConfig config;

    //This tracks the batch number sent for the captions. only increases by 1 each time a caption is sent
    private int transmissionNum;

    /*
    Unfortunately, Google cuts its stream after 5 minutes (300000 milliseconds) and must be restarted in order to function. These variables facilitate that
    */
    private ArrayList<com.google.protobuf.ByteString> currentAudioInput = new ArrayList<com.google.protobuf.ByteString>(); // Stores the audio since the last transcription
    // Since the data fed to the current transcription is about to be lost - or was lost - we must keep all data since the last transcription
    private ArrayList<com.google.protobuf.ByteString> lastAudioInput = new ArrayList<com.google.protobuf.ByteString>();
    private int resultEndTimeInMS = 0;
    private int isFinalEndTime = 0;
    private int finalRequestEndTime = 0;
    private boolean newStream = true;
    private double bridgingOffset = 0;
    private long streamingLimitStartTime;

    private StreamController referenceToStreamController;

    private ResponseObserver<StreamingRecognizeResponse> responseObserver = null;

    private SpeechClient client;

    private ClientStream<StreamingRecognizeRequest> clientStream;

    private StreamingRecognitionConfig streamingRecognitionConfig;


    /*
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     ************************************************************CONSTRUCTORS and Props************************************************************
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     */
    /**
     * The constructor.
     *
     * @param config The config object.
     */
    public GoogleSttActor(AsrConfig config) {
        this.config = config;
        this.transmissionNum = 0;
    }

    /**
     * The props.
     */
    public static Props props(AsrConfig config) {
        return Props.create(GoogleSttActor.class, () -> new GoogleSttActor(config));
    }

    /*
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     ************************************************************Akka Actor Overrides************************************************************
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     */

    @Override
    public void preStart() {
        logger.debug("Startup GoogleSttActor actor with name [{}].", self().path().name());

        // Start captioning!
        beginCaptioning();
    }

    @Override
    public void postStop() {
        logger.debug("Stopping GoogleSttActor actor with name [{}].", self().path().name());


        try {
            clientStream.closeSend();
        } catch (Exception e) {
            logger.warn("postStop: could not close client stream for Google SpeechToText actor [{}].", self().path().name(), e);
        }
        try {
            referenceToStreamController.cancel(); // remove Observer
        } catch (Exception e) {
            logger.warn("postStop: could not close reference to stream controller for Google SpeechToText actor [{}].", self().path().name(), e);
        }
    }

    /*
     *************************************************************************************************************************************
     *************************************************************************************************************************************
     ************************************************************Class Methods************************************************************
     *************************************************************************************************************************************
     *************************************************************************************************************************************
     */

    /**
     * This method begins the captioning session with Google SpeechToText.
     *
     **/
    public void beginCaptioning() {

        streamingLimitStartTime = System.currentTimeMillis();
        try  {
            client = SpeechClient.create();
            responseObserver =
                    new ResponseObserver<StreamingRecognizeResponse>() {

                        public void onStart(StreamController controller) {
                            referenceToStreamController = controller;
                        }

                        public void onResponse(StreamingRecognizeResponse response) {
                            StreamingRecognitionResult result = response.getResultsList().get(0);


                            SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
                            Duration resultEndTime = result.getResultEndTime();
                            // this line is necessary for the byteString readjustment to work later
                            resultEndTimeInMS = (int) ((resultEndTime.getSeconds() * 1000) + (resultEndTime.getNanos() / 1000000));

                            String transcript = alternative.getTranscript().trim();
                            if (result.getIsFinal() && (transcript != null) && !(transcript.equals(""))) {

                                //logger.info("BRENT - language code: [{}] confidence: [{}].", result.getLanguageCode(), alternative.getConfidence());

                                isFinalEndTime = resultEndTimeInMS;

                                int wordCount = transcript.split("\\s").length; //.getWordsCount() returns 0 and getWordsList() returns an empty list
                                double tempConfidenceSum = alternative.getConfidence()*wordCount; // Unfortunately other vendors just do confidence by word; we have to mimic this
                                // Later on in the code this will be divided by number of words so this will be set back again

                                if (context() != null ) {
                                    logger.info("Google word count: [{}] confidence: [{}] language code: [{}] transcript: [{}]", wordCount, alternative.getConfidence(), result.getLanguageCode(), transcript);
                                    transmissionNum++;
                                }
                            }
                        }

                        public void onComplete() {}

                        public void onError(Throwable t) {}
                    };

            clientStream = client.streamingRecognizeCallable().splitCall(responseObserver);

            //it is important to note that Google cannot use a sample rate of 8k - it needs at least 16. If it is 8, we must make it 16 here and in the sample rate itself
            int sampleRate = this.config.getAudioFormatSampleRate();//((this.config.getAudioFormatSampleRate() == 8000)?2:1)*this.config.getAudioFormatSampleRate();

            // Make the list of alternative languages that language detection can use
            ArrayList<String> languageList = new ArrayList<>();
            languageList.add("ja-JP");
            languageList.add("es-ES");
            //languageList.add("es-US");
            languageList.add("en-US");

            RecognitionConfig recognitionConfig =
                    RecognitionConfig.newBuilder()
                            .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                            .setLanguageCode("en-US")
                            //.addAllAlternativeLanguageCodes(languageList) // in order for this to work, the beta must be used (see above) AND the model MUST be "default" - any other model makes it bomb out!
                            .setSampleRateHertz(sampleRate)
                            .setEnableAutomaticPunctuation(true)
                            .setProfanityFilter(false)
                            .setModel(config.getGoogleTranscriptionModel())
                            .build();

            streamingRecognitionConfig =
                    StreamingRecognitionConfig.newBuilder()
                            .setConfig(recognitionConfig)
                            .setInterimResults(true)
                            .build();

            StreamingRecognizeRequest request =
                    StreamingRecognizeRequest.newBuilder()
                            .setStreamingConfig(streamingRecognitionConfig)
                            .build(); // The first request in a streaming call has to be a config

            clientStream.send(request);

            logger.info("Google connection complete - [{}] ready to start captions using transcription model [{}]!", self().path().name(), config.getGoogleTranscriptionModel());

            // Google Speech to text is now ready to caption!
        } catch (Exception e) {
            logger.error("beginCaptioning: Google Transcription error in [{}]: [{}].", self().path().name(), e.getMessage(), e);
        }
    }

    /*
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     ************************************************************Akka Receive Handlers************************************************************
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     */

    /**
     * This method simply acts as a receiver for messages we know we will get but do not care to do anything with.
     * @param msg The received message.
     */
    private void doNothing(Object msg) {

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This handles the actual payload from the stream.
     *
     * ###WARNING### This is different than AWS / IBM handleIncomingPacket
     *
     * @param packet A ByteString that is the payload.
     */
    private void handleIncomingPacket(ByteString packet) {

        byte[] data;
        byte[] rawData = packet.toArray();
        com.google.protobuf.ByteString tempByteString;
        StreamingRecognizeRequest request;

        // we do not have to do anything like ripping rtp headers off or decoding from ulaw to pcmu - we let the asr microservice do this and then it sends the raw data
        // to us, so we do not have to do any postprocessing of the data
        data = rawData;

        try {
            tempByteString = com.google.protobuf.ByteString.copyFrom(data);
            currentAudioInput.add(tempByteString);

            /*
            From here down to the other note was written by Google here: https://github.com/googleapis/java-speech/blob/master/samples/snippets/src/main/java/com/example/speech/InfiniteStreamRecognize.java
            This deals with the fact that the client cuts off after 5 minutes
            */
            if ((System.currentTimeMillis() - streamingLimitStartTime) >= config.getGoogleReconnectTime()) {
                logger.debug("Google client stream turnover for [{}]", self().path().name());
                clientStream.closeSend();
                referenceToStreamController.cancel(); // remove Observer. Its re-added in below in the splitCall line

                if (resultEndTimeInMS > 0) {
                    finalRequestEndTime = isFinalEndTime;
                }
                resultEndTimeInMS = 0;

                lastAudioInput = null;
                lastAudioInput = currentAudioInput;
                currentAudioInput = new ArrayList<com.google.protobuf.ByteString>();

                newStream = true;

                clientStream = client.streamingRecognizeCallable().splitCall(responseObserver);
                request = StreamingRecognizeRequest.newBuilder().setStreamingConfig(streamingRecognitionConfig).build();
                streamingLimitStartTime = System.currentTimeMillis();

                // block until clientStream comes back
                while ((clientStream.isSendReady() == false) && ((System.currentTimeMillis() - streamingLimitStartTime) < config.getGoogleClientStreamBlockingTime())) { }

                // if we actually hit the block time
                long totalBlockedTime = (System.currentTimeMillis() - streamingLimitStartTime);
                if (totalBlockedTime >= config.getGoogleClientStreamBlockingTime()) {
                    logger.warn("Exited blocking Google client stream before client was ready for [{}]", self().path().name());
                } else logger.debug("Google stream turnover: [{}] back online after [{}] milliseconds.", self().path().name(), totalBlockedTime);

                clientStream.send(request);
            } else {

                if ((newStream) && (lastAudioInput.size() > 0)) {
                    // if this is the first audio from a new request, calculate amount of unfinalized audio from last request
                    // resend the audio to the speech client before incoming audio
                    double chunkTime = config.getGoogleReconnectTime() / lastAudioInput.size();
                    // ms length of each chunk in previous request audio arrayList
                    if (chunkTime != 0) {
                        if (bridgingOffset < 0) {
                            // bridging Offset accounts for time of resent audio calculated from last request
                            bridgingOffset = 0;
                        }
                        if (bridgingOffset > finalRequestEndTime) {
                            bridgingOffset = finalRequestEndTime;
                        }
                        // chunks from MS is number of chunks to resend
                        int chunksFromMS = (int) Math.floor((finalRequestEndTime - bridgingOffset) / chunkTime);

                        // set bridging offset for next request
                        bridgingOffset = (int) Math.floor((lastAudioInput.size() - chunksFromMS) * chunkTime);

                        for (int i = chunksFromMS; i < lastAudioInput.size(); i++) {
                            request = StreamingRecognizeRequest.newBuilder().setAudioContent(lastAudioInput.get(i)).build();
                            //if (clientStream.isSendReady() == false ) logger.error("New stream: Client stream not ready to send for Google in [{}].", self().path().name());
                            clientStream.send(request);
                        }
                    }
                    newStream = false;
                }
                request = StreamingRecognizeRequest.newBuilder().setAudioContent(tempByteString).build();

                //if (clientStream.isSendReady() == false ) logger.error("Normal Channel: Client stream not ready to send for Google in [{}].", self().path().name());
                clientStream.send(request);
            }
            /*
            This ends the Google section of code.
            */

        } catch (Exception e) {
            // something legitimately happened to the stream - limit to one message a minute in the log
            // if its been more than a minute since our last message to the log
                logger.error("Error in Google stream [{}]: [{}]", self().path().name(), e.getMessage(), e);
        }

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This method is launched if there is an error in the stream.
     * @param failure A throwable failure.
     */
    private void handleStreamError(ActorSinkMsgs.StreamFailure failure) {
        logger.error("Error in stream: [{}].", failure.getCause(), failure);

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(ByteString.class, msg -> handleIncomingPacket(msg))
                .match(ActorSinkMsgs.StreamInitialized.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamCompleted.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamFailure.class, msg -> handleStreamError(msg))

                .build();
    }

    /*
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     ************************************************************Static Methods************************************************************
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     */

    /**
     * Get the prefix for this type of actor.
     *
     * @return The prefix for this actor.
     */
    public static String getActorPrefix() { return "ASRGOOGLE"; }

    /**
     * Accepts a caption bridge ID, strips off the captionBridgeID prefix, and then returns an appropriate name for this actor.
     *
     * @param captionBridgeID The associated captionBridge ID in Asterisk for this caption session.
     * @return The proposed name for this actor.
     */
    public static String getProposedActorName(String captionBridgeID) { return getActorPrefix() + captionBridgeID.substring(2); }
}