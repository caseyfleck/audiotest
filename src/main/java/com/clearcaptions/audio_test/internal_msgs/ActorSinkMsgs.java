package com.clearcaptions.audio_test.internal_msgs;

/**
 * These messages handle all messages required by actors that act as sinks in streams (specifically, Sink.actorRefWithBackpressure actors).
 *
 * It should be noted there is at least one more message sent to the Sink.actorRefWithBackpressure: the payload itself (which is usually of type ByteString.class).
 */
public class ActorSinkMsgs {

    /**
     *  The Ack enum is used by Sink.actorRefWithBackpressure to acknowledge it received the packet. This is critical, as without an Ack backpressure does not work in a stream that uses
     *  an actor as a sink.
     */
    public enum Ack {
        ACKNOWLEDGE;
    }

    /**
     * The message sent to the actor when the stream initializes.
     */
    public static class StreamInitialized {}

    /**
     * The message sent to the actor when the stream completes.
     */
    public static class StreamCompleted {}

    /**
     * This message is used for stream failures. Its constructor takes a Throwable object.
     */
    public static class StreamFailure {
        private final Throwable cause;

        public StreamFailure(Throwable cause) {
            this.cause = cause;
        }

        public Throwable getCause() {
            return cause;
        }
    }
}
