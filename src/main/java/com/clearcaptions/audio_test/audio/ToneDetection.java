package com.clearcaptions.audio_test.audio;

import akka.actor.ActorRef;
import org.jtransforms.fft.FloatFFT_1D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This was heavily influenced by DSPSoundManager in the peers library.
 */

public class ToneDetection {

    private static final Logger logger = LoggerFactory.getLogger(ToneDetection.class);
    public static final int DEFAULT_SAMPLE_SIZE_BYTES = 1;
    public static final int DEFAULT_FRAME_SIZE = 1;
    public static final int DEFAULT_CLOCK = 8000; // Hz
    private FloatFFT_1D fft;
    private final int windowSizeMS;
    private int sampleRate, sampleSize, frameSize;
    private int bufferSize;
    private float[] buffer;
    private int bufferIndex=0;
    private float minLevel;
    private int lastFrequencyDetected=0;
    private DTMF lastDtmfDetected=null;
    private int countryCode;

    private ActorRef associatedAsrCaptionSessionActor;

    private boolean newToneAvailable;

    public ToneDetection(int sampleRate, int sampleSizeBytes, int frameSize, int windowSizeMS, int threshold, int countryCode, ActorRef associatedAsrCaptionSessionActor) {
        this.sampleRate=sampleRate;
        this.sampleSize=sampleSizeBytes;
        this.frameSize = frameSize;
        this.windowSizeMS = windowSizeMS;
        this.minLevel = threshold;
        this.countryCode = countryCode;

        this.associatedAsrCaptionSessionActor = associatedAsrCaptionSessionActor;

        this.bufferSize=Math.round((float)windowSizeMS/1000.0F*(sampleRate*frameSize*sampleSize));
        this.buffer=new float[bufferSize];
        fft=new FloatFFT_1D(bufferSize);

        newToneAvailable = false;
    }


    protected void onBufferFull(float[] buffer) {
        //logger.trace("{} buffer is full, running fft ...", getName());
        // https://github.com/wendykierp/JTransforms/issues/4
        fft.realForward(buffer);
        // Extract Real part
        float localMax = Float.MIN_VALUE;
        int maxValueFreq = -1, nextMaxValueFreq=-1;
        float[] result = new float[buffer.length / 2];

        // look for the max in the frequency buckets
        for(int s = 0; s < result.length; s++) {
            //result[s] = Math.abs(signal[2*s]);
            float re = buffer[s * 2];
            float im = buffer[(s * 2) + 1];
            result[s] = (float) Math.sqrt(re * re + im * im) / result.length;
            if(result[s] > localMax) {
                nextMaxValueFreq = maxValueFreq;
                maxValueFreq = s * 2; // TODO this *2 shouldn't be needed
            }
            localMax = Math.max(localMax, result[s]);
        }
        //logger.trace("{} loudest frequency was {}hz at {}", getName(), maxValueFreq, localMax);
        if (localMax >= minLevel) {
            if (lastFrequencyDetected != maxValueFreq) {
                if (nextMaxValueFreq>0) {
                    lastDtmfDetected=new DTMF(countryCode, maxValueFreq, nextMaxValueFreq);

                    // set the new tone available flag
                    newToneAvailable = true;
                    //logger.warn("{} detected dtmf tone: {}", associatedAsrCaptionSessionActor.path().name(), lastDtmfDetected);
                }
                //logger.debug("{} detected {}hz at {} (followed by {}hz)", getName(), maxValueFreq, localMax, nextMaxValueFreq);
            }
            lastFrequencyDetected = maxValueFreq;
        } else {
            //logger.trace("no frequencies detected at or above {} threshold", minLevel);
        }
    }

    public int writeData(byte[] buffer, int offset, int length) {
        //set a temp buffer index to the index
        int bidx=bufferIndex;

        //go the length of the new buffer we are placing on the buffer and ALSO increase the temp buffer index each time
        for (int i=0; i<length; i++, bidx++) {

            // IF the temp buffer index is more than the allowable buffer size, send the entire buffer to onBufferFull and then reset the temp buffer index
            if (bidx>=this.bufferSize) {
                onBufferFull(this.buffer);
                bidx=0;
            }

            //set the position in the buffer to the temp index and then assign that index in the real buffer the associated value from the temp buffer
            this.buffer[bidx]=(float)buffer[offset+i];
        }

        //re-set the real buffer index to the temp index
        bufferIndex=bidx;
        return length;
    }

    public int getLastFrequencyDetected() {
        return lastFrequencyDetected;
    }

    public DTMF getLastDtmfDetected() {
        return lastDtmfDetected;
    }

    public void acknowledgeNewTone() { newToneAvailable = false; }

    public boolean isNewToneAvailable() { return newToneAvailable; }

    /**
     * Some vendors - like AWS - mis-interpret a busy tone as 'uh', 'four', 'for', 4, repeated. IF the transcription is comprised ENTIRELY of these markers, return true; false otherwise.
     * @param potentialPhrase
     * @return
     */
    public static boolean potentialBusyToneTranscription(String potentialPhrase) {
        String tempString = potentialPhrase;

        tempString = tempString.toLowerCase();

        //get rid of the phrases and characters that would be in a string that mis-translates a fast busy signal
        tempString = tempString.replaceAll("for", " ");
        tempString = tempString.replaceAll("four", " ");
        tempString = tempString.replaceAll("uh", " ");
        tempString = tempString.replaceAll("[\\.4]", " ");

        //trim the phrase
        tempString = tempString.trim();

        if (tempString.equals("")) return true;
        else return false;
    }

    /**
     * Some vendors - like AWS - mis-interpret a ringing tone as 'Hm.', 'Mhm.', 'Mm.', etc. IF the transcription is comprised ENTIRELY of these markers, return true; false otherwise.
     * @param potentialPhrase The transcription we are investigating.
     * @return
     */

    public static boolean potentialRingTranscription(String potentialPhrase) {
        String tempString = potentialPhrase;

        tempString = tempString.toLowerCase();

        //get rid of the characters that would be in a string that mis-translates a ringing tone
        tempString = tempString.replaceAll("[\\.hm]", " ");

        //trim the phrase
        tempString = tempString.trim();

        if (tempString.equals("")) return true;
        else return false;
    }
}
