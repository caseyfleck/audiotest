package com.clearcaptions.audio_test.audio;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.util.ByteString;
import com.clearcaptions.audio_test.config.AsrConfig;
import com.clearcaptions.audio_test.internal_msgs.ActorSinkMsgs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;

/**
 * This actor's main purpose is to deal with all of the auxiliary audio tasks; for example, tone demodulation (DTMF tasks), potentially forwarding on the stream to another destination, etc.
 * Currently, this actor:
 *      * Monitors UDP stream activity coming from Asterisk.
 *      * Determines DTMT tones from the stream.
 *      * Forwards the audio stream to a specified IP.
 *        * For this to work with the ASR microservice, you MUST add your IP:userID to the parameter 'test.send-audio-to-ip' in /opt/cc/microservices/asr/features.conf. In addition, you must make sure
 *          both 'test.test-mode-enabled' and 'test.enable-audio-to-ip' are set to 'true' in the same file.
 *        * You can use the 'asr-audio-testing' app in bitbucket to monitor the audio if you wish.
 */
public class AuxiliaryAudioActor extends AbstractActor {

    private static final Logger logger = LoggerFactory.getLogger(AuxiliaryAudioActor.class);

    // Since this actor will only last a few minutes, this config actor should not change
    private AsrConfig config;

    // These track to see if we have recorded a ringing tone or a busy signal tone from Akka, respectively
    private boolean ringingRecorded;

    ToneDetection toneDetection;

    /*
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     ************************************************************CONSTRUCTORS and Props************************************************************
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     */

    public AuxiliaryAudioActor(AsrConfig config) {

        this.config = config;

        ringingRecorded = false;
    }

    public static Props props(AsrConfig config) {
        return Props.create(AuxiliaryAudioActor.class, () -> new AuxiliaryAudioActor(config));
    }

    /*
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     ************************************************************Akka Actor Overrides************************************************************
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     */
    @Override
    public void preStart() {

        toneDetection = new ToneDetection(this.config.getAudioFormatSampleRate(),
                this.config.getAudioFormatSampleSizeInBits()/8,
                1, // if different than 1 the tones could be off
                500, // if different than 500 the tones could be off
                5,
                1,
                self());

    }


    @Override
    public void postStop() {

    }

    @Override
    public void preRestart(Throwable reason, Optional<Object> message) throws Exception {
        logger.error("preRestart: AuxiliaryAudioActor {} has encountered some catastrophic error and had to be re-started in method [{}].", self().path().name(), message.get(), reason);

    }

    /*
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     ************************************************************Akka Receive Handlers************************************************************
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     */

    /**
     * This method simply acts as a receiver for messages we know we will get but do not care to do anything with.
     * @param msg The received message.
     */
    private void doNothing(Object msg) {

        //logger.debug("Some message received!");

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This handles the actual payload from the stream.
     * @param packet A ByteString that is the payload.
     */
    private void handleIncomingPacket(ByteString packet) {
        byte[] data;
        byte[] rawData = packet.toArray();

        //logger.debug("Data received for sink [{}]: [{}].", self().path(), rawData);

        // we do not have to do anything like ripping rtp headers off or decoding from ulaw to pcmu - we let the asr microservice do this and then it sends the raw data
        // to us, so we do not have to do any postprocessing of the data
        data = rawData;

        toneDetection.writeData(data, 0, data.length);

        // if there is a new tone available
        if (toneDetection.isNewToneAvailable()) {

            DTMF dtmf = toneDetection.getLastDtmfDetected();
            if ((dtmf != null) && (ringingRecorded == false) && (dtmf.isRinging(5))) {
                ringingRecorded = true;
                logger.debug("Ringing observed!");
            } else if ((dtmf != null) && (dtmf.isBusySignal(5))) {
                logger.debug("Busy signal observed!");
            }
            toneDetection.acknowledgeNewTone();
        }

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This method is launched if there is an error in the stream.
     * @param failure A throwable failure.
     */
    private void handleStreamError(ActorSinkMsgs.StreamFailure failure) {
        logger.error("Error in stream: [{}].", failure.getCause(), failure);

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }


    @Override
    public AbstractActor.Receive createReceive() {
        return  receiveBuilder()
                .match(ByteString.class, msg -> handleIncomingPacket(msg))
                .match(ActorSinkMsgs.StreamInitialized.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamCompleted.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamFailure.class, msg -> handleStreamError(msg))
                .build();
    }

    /*
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     ************************************************************Static Methods************************************************************
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     */

    /**
     * Get the prefix for this type of actor.
     *
     * @return The prefix for this actor.
     */
    public static String getPrefix() { return "asr.AuxAudioActor."; }

    /**
     * Accepts a caption bridge ID, strips off the captionBridgeID prefix, and then returns an appropriate name for this actor.
     *
     * @param captionBridgeID The associated captionBridge ID in Asterisk for this caption session.
     * @return The proposed name for this actor.
     */
    public static String getProposedActorName(String captionBridgeID) { return getPrefix() + captionBridgeID.substring(2); }
}
