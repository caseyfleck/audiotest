package com.clearcaptions.audio_test.audio;

/**
 * This class exists because we found it was necessary to upsample. There is no need to instantiate this class, just use its methods.
 */
public class Resample {

    /**
     * Sometimes we wish to upside the sample rate (say, from 8k to 16k) - this method does so.
     * This method was pulled from https://stackoverflow.com/questions/3260424/resample-upsample-sound-frames-from-8khz-to-48khz-java-android .
     * This is the example given of an upsampling by a factor of 6 (8Khz to 48Khz), where 'i' is the byte in the sample and 'n' is the byte length in the sample:
     *   for (i=0; i<n-1; i++)
     *   {
     *     output[i*6+0] = input[i]*6/6 + input[i+1]*0/6;
     *     output[i*6+1] = input[i]*5/6 + input[i+1]*1/6;
     *     output[i*6+2] = input[i]*4/6 + input[i+1]*2/6;
     *     output[i*6+3] = input[i]*3/6 + input[i+1]*3/6;
     *     output[i*6+4] = input[i]*2/6 + input[i+1]*4/6;
     *     output[i*6+5] = input[i]*1/6 + input[i+1]*5/6;
     *   }
     *
     * @param factor The factor we are upsampling (for example, going from 8Khz to 16Khz is a factor of 2; going from 8Khz to 48Khz is a factor of 6)
     * @param data The upsampled data returned.
     * @return
     */
    public static byte[] upsampleViaLinearInterpolation(int factor, byte[] data) {

        byte[] resampledData = new byte[data.length*2];
        for (int i=0; i<(data.length-1); i++) {
            for (int x=0; x<factor; x++) resampledData[i * factor + x] = (byte) ((data[i] * ((float) factor - x) / factor) + ((data[i + 1] * (float) x) / factor));
        }

        return resampledData;
    }

    /**
     * This method only finds 1 additional point, so its restricted to a factor of 2.
     *
     * @param data the byte array.
     * @return The modified byte array,
     */
    public static byte[] upsampleViaAveragingDataPoints(byte[] data, byte lastDataByte) {

        byte[] resampledData = new byte[data.length*2];
        for (int i=0; i<=(data.length-1); i++) {
            //used to be simply:
            //resampledData[(i * 2) + 0] = data[i];
            //resampledData[(i * 2) + 1] = (byte) ((int) ((data[i+1] + data[i]) / 2));

            if (lastDataByte == 0 && i == 0) {
                //this is the very first byte - we cannot average anything previously so just use the first datapoint twice
                resampledData[(i * 2) + 0] = data[i];
                resampledData[(i * 2) + 1] = data[i];
            } else if (lastDataByte != 0 && i == 0) {
                //this is mid-stream somewhere - we need to average the lastDataByte with the first in this series
                resampledData[(i * 2) + 0] = (byte) ((int) ((lastDataByte + data[i]) / 2));
                resampledData[(i * 2) + 1] = data[i];
            } else {
                //this is mid-this stream
                resampledData[(i * 2) + 0] = (byte) ((int) ((data[i-1] + data[i]) / 2));
                resampledData[(i * 2) + 1] = data[i];

            }
        }

        //used to simply be (NOTE: this was in when the above used a < and NOT a <=)
        ////we have not dealt with the last byte, so do so now
        //resampledData[((data.length-1) * 2) + 0] = (byte) ((int) ((data[data.length-2] + data[data.length-1]) / 2));
        //resampledData[((data.length-1) * 2) + 1] = data[data.length-1];

        return resampledData;
    }

    /**
     * It is important to note that some ASR vendors (notably IBM) cannot use a sample rate of 8k - it needs at least 16k. In order to do this, we can simply repeat packets
     * to build the sample rate up. For example, If the sample rate is 8k, and we need 16k, we can just repeat every packet we get twice (a factor of 2); if the rate is 16k and we need 48,
     * we can repeat every packet 3 times (16k * 3 = 48k)
     *
     *
     * @param data The data in byte[] format.
     * @param factor The factor by which we want to increase the sample size. For example, if we are going from 8k and we want 16k, that is a factor of 2 (doubling).
     * @return The modified sampled data.
     */
    public static byte[] upsampleViaRepeating(byte[] data, int factor) {
        byte[] resampledData = new byte[data.length*2];
        for (int i=0; i<=(data.length-1); i++) {
            for (int x = 0;x<factor;x++) resampledData[(i * factor) + x] = data[i];
        }

        return resampledData;
    }

    /**
     * This was an upsample idea found on stackoverflow.  The idea is it upsamples by a factor of two but _also_ allegedly smoothes the audio as it uses a sliding window to average
     * 3 data points at a time (giving the _original_ data point more weight).
     *
     * The idea was found here: https://stackoverflow.com/questions/15410725/java-resample-wav-soundfile-without-third-party-library
     *
     * @param data
     * @return
     */
    public static byte[] upsampleEnahnceAudio(byte[] data) {

        byte[] resampledData = new byte[data.length*2];

        for (int i=0; i<(data.length-2); i++) {

            if (i==0) {
                // if this is the first byte, simply copy the byte
                resampledData[0] = data[0];
                resampledData[1] = (byte) ((int) ((data[i+1] + data[i]) / 2)); // create a new byte, averaged with this data point and the next point
            } else {
                resampledData[(i * 2) + 0] = (byte) ((data[i-1] + 2*data[i] + data[i+1])/4); //we modify the original byte for better sound, using the point before and after; weight the original point more
                resampledData[(i * 2) + 1] = (byte) ((int) ((data[i+1] + data[i]) / 2)); // create a new byte, averaged with this data point and the next point
            }
        }

        //we still have to accomodate for the last byte (and the new point before it)
        resampledData[((data.length-1) * 2) + 0] = (byte) ((data[(data.length-1)-1] + 2*data[(data.length-1)])/3); //we modify the original byte for better sound, using the point before; weight the original point more
        resampledData[((data.length-1) * 2) + 1] = data[(data.length-1)-1]; // create a new byte, simply using the last byte

        return resampledData;
    }
}
