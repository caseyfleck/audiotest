package com.clearcaptions.audio_test.audio;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.util.ByteString;
import com.clearcaptions.audio_test.config.AsrConfig;
import com.clearcaptions.audio_test.internal_msgs.ActorSinkMsgs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sound.sampled.*;

/**
 * This class is strictly used for testing both incoming streams and audio settings by playing the stream on your speakers. It is not meant to be active while in production - its for
 * testing only!
 *
 * To set this, alter the following in features.conf:
 *      * test.enable-speakers:true
 */
public class SpeakerActorSink extends AbstractActor {

    private static final Logger logger = LoggerFactory.getLogger(SpeakerActorSink.class);

    // Since this actor will only last a few minutes, this config actor should not change
    private AsrConfig config;

    // The object that will control the speakers
    private SourceDataLine speakers;

    /*
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     ************************************************************CONSTRUCTORS and Props************************************************************
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     */

    public SpeakerActorSink(AsrConfig config) {

        logger.info("Speakers are engaged.");

        this.config = config;

        AudioFormat audioFormat = new AudioFormat(this.config.getAudioFormatSampleRate(), this.config.getAudioFormatSampleSizeInBits(), this.config.getAudioFormatChannels(), this.config.getAudioFormatPcmSigned(), this.config.getAudioFormatBigEndian());

        try {
            DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, audioFormat);
            speakers = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
            speakers.open(audioFormat);
            speakers.start();
        } catch (LineUnavailableException e) {
            logger.error("Problem opening the speakers: [{}].", e.getMessage(), e);
        }
    }

    public static Props props(AsrConfig config) {
        return Props.create(SpeakerActorSink.class, () -> new SpeakerActorSink(config));
    }

    /*
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     ************************************************************Akka Actor Overrides************************************************************
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     */

    @Override
    public void postStop() {

        //close the speaker object
        try {
            speakers.stop();
            speakers.close();
        } catch (Exception e) {}
    }

    /*
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     ************************************************************Akka Receive Handlers************************************************************
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     */

    /**
     * This method simply acts as a receiver for messages we know we will get but do not care to do anything with.
     * @param msg The received message.
     */
    private void doNothing(Object msg) {

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This handles the actual payload from the stream.
     * @param packet A ByteString that is the payload.
     */
    private void handleIncomingPacket(ByteString packet) {
        byte[] data;
        byte[] rawData = packet.toArray();

        //logger.debug("Data received: [{}].", rawData);

        // we do not have to do anything like ripping rtp headers off or decoding from ulaw to pcmu - we let the asr microservice do this and then it sends the raw data
        // to us, so we do not have to do any postprocessing of the data
        data = rawData;

        // actually write to the speakers
        speakers.write(data, 0, data.length);

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This method is launched if there is an error in the stream.
     * @param failure A throwable failure.
     */
    private void handleStreamError(ActorSinkMsgs.StreamFailure failure) {
        logger.error("Error in SpeakerActorSink in the stream: [{}].", failure.getCause(), failure);

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return  receiveBuilder()
                .match(ByteString.class, msg -> handleIncomingPacket(msg))
                .match(ActorSinkMsgs.StreamInitialized.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamCompleted.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamFailure.class, msg -> handleStreamError(msg))
                .build();
    }
}

