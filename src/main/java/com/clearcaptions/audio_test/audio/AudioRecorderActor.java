package com.clearcaptions.audio_test.audio;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.util.ByteString;
import com.clearcaptions.audio_test.config.AsrConfig;
import com.clearcaptions.audio_test.internal_msgs.ActorSinkMsgs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sound.sampled.*;
import java.io.File;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * This class is strictly used for testing both incoming streams and audio settings by playing the stream on your speakers. It is not meant to be active while in production - its for
 * testing only!
 *
 * To set this, alter the following in features.conf:
 *      * test.enable-speakers:true
 */
public class AudioRecorderActor extends AbstractActor {

    private static final Logger logger = LoggerFactory.getLogger(AudioRecorderActor.class);

    // Since this actor will only last a few minutes, this config actor should not change
    private AsrConfig config;

    private File wavFile; // path of the wav file
    private final AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE; // format of audio file
    private int fileNum;

    PipedOutputStream filePipedOut;
    PipedInputStream filePipedIn; // line
    AudioInputStream fileAudioInputStream = null; // ais

    private boolean dirExists = false;

    /*
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     ************************************************************CONSTRUCTORS and Props************************************************************
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     */

    public AudioRecorderActor(AsrConfig config) {

        logger.info("Audio Recorder engaged.");

        this.config = config;
        this.fileNum = 0;
        filePipedOut=new PipedOutputStream();

        File wavDirectory = new File(config.getMyAudioFileDirectory());

        dirExists = wavDirectory.exists();

        if (dirExists) {
            wavFile = new File(config.getMyAudioFileDirectory() + "/" + config.getMyAudioFileName() + this.fileNum + ".wav");
            // find the first free file
            while (wavFile.exists()) {
                this.fileNum++;
                wavFile = new File(config.getMyAudioFileDirectory() + "/" + config.getMyAudioFileName() + this.fileNum + ".wav");
            }

            logger.info("Audio file will be saved as: {}",wavFile.getAbsolutePath());

            AudioFormat audioFormat = new AudioFormat(this.config.getAudioFormatSampleRate(), this.config.getAudioFormatSampleSizeInBits(), this.config.getAudioFormatChannels(), this.config.getAudioFormatPcmSigned(), this.config.getAudioFormatBigEndian());

            try {
                filePipedIn = new PipedInputStream(filePipedOut, config.getAudioFormatInputStreamPipeSize());
            } catch (java.io.IOException e) {
                logger.error("Could not open the PipedInputStream in AudioRecorderActor!");
            }

            fileAudioInputStream = new AudioInputStream(filePipedIn, audioFormat, AudioSystem.NOT_SPECIFIED);


        } else logger.error("Directory [{}] does not exist - cannot save audio files!", config.getMyAudioFileDirectory());

    }

    public static Props props(AsrConfig config) {
        return Props.create(AudioRecorderActor.class, () -> new AudioRecorderActor(config));
    }

    /*
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     ************************************************************Akka Actor Overrides************************************************************
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     */

    @Override
    public void preStart() {
        // The functionality of AudioSystem.write blocks when used with a PipedInputStream / PipedOutputStream setup - in order to get around this, we create - and then
        // sacrifice - a child.
        context().actorOf(AudioRecorderChild.props(fileAudioInputStream, fileType, wavFile), "my.AudioRecorderChild");
    }

    @Override
    public void postStop() {

        //close the objects
        try {
            filePipedOut.close();
            filePipedIn.close();
        } catch (Exception e) {
        }
    }

    /*
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     ************************************************************Akka Receive Handlers************************************************************
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     */

    /**
     * This method simply acts as a receiver for messages we know we will get but do not care to do anything with.
     *
     * @param msg The received message.
     */
    private void doNothing(Object msg) {

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This handles the actual payload from the stream.
     *
     * @param packet A ByteString that is the payload.
     */
    private void handleIncomingPacket(ByteString packet) {
        byte[] data;
        byte[] rawData = packet.toArray();

        // we do not have to do anything like ripping rtp headers off or decoding from ulaw to pcmu - we let the asr microservice do this and then it sends the raw data
        // to us, so we do not have to do any postprocessing of the data
        data = rawData;

        // write to the pipedOutputStream for the file
        try {
            filePipedOut.write(data);
        } catch (java.io.IOException e) {
            logger.error("Could not write to audio file!");
        }

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This method is launched if there is an error in the stream.
     *
     * @param failure A throwable failure.
     */
    private void handleStreamError(ActorSinkMsgs.StreamFailure failure) {
        logger.error("Error in AudioRecorderActor in the stream: [{}].", failure.getCause(), failure);

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(ByteString.class, msg -> handleIncomingPacket(msg))
                .match(ActorSinkMsgs.StreamInitialized.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamCompleted.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamFailure.class, msg -> handleStreamError(msg))
                .build();
    }

}