package com.clearcaptions.audio_test.audio;

import java.util.HashMap;
import java.util.Map;

/**
 * This class performs a few frequency checks. It is based off the sip peers DTMF class but has added features.
 *
 * It should be noted that tones are different per region - hence, we have different regions in this document.
 *
 * Dial tones: https://en.wikipedia.org/wiki/Call-progress_tone
 */
public class DTMF {

    // These are north American digits - right now we do not have the digit frequencies for any other region.
    private static final int[] DIGIT_TONES_LOWER_RANGE=new int[]{941,697,697,697,770,770,770,852,852,852};
    private static final int[] DIGIT_TONES_UPPER_RANGE=new int[]{1336,1209,1336,1477,1209,1336,1477,1209,1336,1477};

    public static int NA_REGION = 1; // North America
    public static int UK_REGION = 2; // UK
    public static int IRELAND_REGION = 3; // Ireland
    public static int EUROPE_REGION = 4; // Europe, NOT UK or Ireland

    private final int minFrequency, maxFrequency;
    private final int region;

    private Map<Integer, Integer> countryCodeToRegion = new HashMap<>(); // a map that uses the area code as a key and the region as a value

    /**
     * Constructor that sets the region automatically to the US and Canada.
     *
     * @param frequency1
     * @param frequency2
     */
    public DTMF(int countryCode, int frequency1, int frequency2) {
        this.minFrequency = Math.min(frequency1, frequency2);
        this.maxFrequency = Math.max(frequency1, frequency2);

        //get the region from the countryCode
        Integer tempRegion = countryCodeToRegion.get(countryCode);
        // if the region was null, assume North America
        region = ((tempRegion == null)?NA_REGION:tempRegion.intValue());

    }

    /**
     * This populates the map that looks up a country code and tells us its internal region code.
     */
    public void populateRegions() {
        //North America
        countryCodeToRegion.put(1, NA_REGION);

        // UK
        countryCodeToRegion.put(44, UK_REGION);

        // Ireland
        countryCodeToRegion.put(353, IRELAND_REGION);

        // Europe
        countryCodeToRegion.put(7, EUROPE_REGION);
        countryCodeToRegion.put(30, EUROPE_REGION);
        countryCodeToRegion.put(31, EUROPE_REGION);
        countryCodeToRegion.put(32, EUROPE_REGION);
        countryCodeToRegion.put(33, EUROPE_REGION);
        countryCodeToRegion.put(34, EUROPE_REGION);
        countryCodeToRegion.put(36, EUROPE_REGION);
        countryCodeToRegion.put(39, EUROPE_REGION);
        countryCodeToRegion.put(40, EUROPE_REGION);
        countryCodeToRegion.put(41, EUROPE_REGION);
        countryCodeToRegion.put(43, EUROPE_REGION);
        countryCodeToRegion.put(45, EUROPE_REGION);
        countryCodeToRegion.put(46, EUROPE_REGION);
        countryCodeToRegion.put(47, EUROPE_REGION);
        countryCodeToRegion.put(48, EUROPE_REGION);
        countryCodeToRegion.put(49, EUROPE_REGION);
        countryCodeToRegion.put(90, EUROPE_REGION);
        countryCodeToRegion.put(298, EUROPE_REGION);
        countryCodeToRegion.put(350, EUROPE_REGION);
        countryCodeToRegion.put(351, EUROPE_REGION);
        countryCodeToRegion.put(352, EUROPE_REGION);
        countryCodeToRegion.put(354, EUROPE_REGION);
        countryCodeToRegion.put(355, EUROPE_REGION);
        countryCodeToRegion.put(356, EUROPE_REGION);
        countryCodeToRegion.put(357, EUROPE_REGION);
        countryCodeToRegion.put(358, EUROPE_REGION);
        countryCodeToRegion.put(359, EUROPE_REGION);
        countryCodeToRegion.put(370, EUROPE_REGION);
        countryCodeToRegion.put(371, EUROPE_REGION);
        countryCodeToRegion.put(372, EUROPE_REGION);
        countryCodeToRegion.put(373, EUROPE_REGION);
        countryCodeToRegion.put(374, EUROPE_REGION);
        countryCodeToRegion.put(375, EUROPE_REGION);
        countryCodeToRegion.put(376, EUROPE_REGION);
        countryCodeToRegion.put(377, EUROPE_REGION);
        countryCodeToRegion.put(378, EUROPE_REGION);
        countryCodeToRegion.put(379, EUROPE_REGION);
        countryCodeToRegion.put(380, EUROPE_REGION);
        countryCodeToRegion.put(381, EUROPE_REGION);
        countryCodeToRegion.put(382, EUROPE_REGION);
        countryCodeToRegion.put(383, EUROPE_REGION);
        countryCodeToRegion.put(385, EUROPE_REGION);
        countryCodeToRegion.put(386, EUROPE_REGION);
        countryCodeToRegion.put(387, EUROPE_REGION);
        countryCodeToRegion.put(389, EUROPE_REGION);
        countryCodeToRegion.put(420, EUROPE_REGION);
        countryCodeToRegion.put(421, EUROPE_REGION);
        countryCodeToRegion.put(423, EUROPE_REGION);
        countryCodeToRegion.put(994, EUROPE_REGION);
        countryCodeToRegion.put(995, EUROPE_REGION);
        countryCodeToRegion.put(3732, EUROPE_REGION);
        countryCodeToRegion.put(3735, EUROPE_REGION);
        countryCodeToRegion.put(7840, EUROPE_REGION);
        countryCodeToRegion.put(7929, EUROPE_REGION);
        countryCodeToRegion.put(7940, EUROPE_REGION);
        countryCodeToRegion.put(7997, EUROPE_REGION);
        countryCodeToRegion.put(37447, EUROPE_REGION);
        countryCodeToRegion.put(37497, EUROPE_REGION);
        countryCodeToRegion.put(90392, EUROPE_REGION);
        countryCodeToRegion.put(90533, EUROPE_REGION);
        countryCodeToRegion.put(90542, EUROPE_REGION);
        countryCodeToRegion.put(99534, EUROPE_REGION);
        countryCodeToRegion.put(99544, EUROPE_REGION);
        countryCodeToRegion.put(799534, EUROPE_REGION);

    }

    public int getMinFrequency() {
        return minFrequency;
    }

    public int getMaxFrequency() {
        return maxFrequency;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+"[ "+minFrequency+"hz and "+maxFrequency+"hz ]";
    }

    public boolean matches(int expected1, int expected2, int variance) {
        return isMatchingFrequency(minFrequency, Math.min(expected1,expected2), variance) &&
                isMatchingFrequency(maxFrequency, Math.max(expected1,expected2), variance);
    }

    protected static boolean isMatchingFrequency(int actual, int expected, int variance) {
        return Math.abs(actual-expected) < variance;
    }

    public boolean isBusySignal(int variance) {

        if (region == UK_REGION) return matches(400, 400, variance);
        else if ((region == IRELAND_REGION) || (region == EUROPE_REGION)) return matches(425, 425, variance);
        else return matches(480, 620, variance); // Assume north American
    }

    public boolean isRinging(int variance) {

        if ((region == UK_REGION) || (region == IRELAND_REGION)) return matches(400, 450, variance);
        else if (region == EUROPE_REGION) return matches(425, 425, variance);
        else return matches(440,480, variance); // assume North American

    }

    public boolean isDialTone(int variance) {
        if (region == UK_REGION) return matches(350, 450, variance);
        else if ((region == IRELAND_REGION) || (region == EUROPE_REGION)) return matches(425, 425, variance);
        else return matches(350, 440, variance); // assume North American
    }

    public boolean isDigit(int digit, int variance) {
        if (digit < 0 || digit > 9) { throw new IllegalArgumentException("Digit must be between 0 and 9!"); }
        return matches(DIGIT_TONES_LOWER_RANGE[digit],DIGIT_TONES_UPPER_RANGE[digit],variance);
    }

}
