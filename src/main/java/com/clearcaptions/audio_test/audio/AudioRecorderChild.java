package com.clearcaptions.audio_test.audio;

import akka.actor.AbstractActor;
import akka.actor.Props;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.File;

/**
 * This class exists simply because AudioSystem.write blocks when used with a PipedInputStream / PipedOutputStream setup - in order to get around this, we sacrifice a child.
 */

public class AudioRecorderChild extends AbstractActor {

    private static final Logger logger = LoggerFactory.getLogger(AudioRecorderChild.class);

    private AudioInputStream fileAudioInputStream;
    private AudioFileFormat.Type fileType;
    private File wavFile;

    public AudioRecorderChild(AudioInputStream fileAudioInputStream, AudioFileFormat.Type fileType, File wavFile) {
        this.fileAudioInputStream = fileAudioInputStream;
        this.fileType = fileType;
        this.wavFile = wavFile;
    }

    public static Props props(AudioInputStream fileAudioInputStream, AudioFileFormat.Type fileType, File wavFile) {
        return Props.create(AudioRecorderChild.class, () -> new AudioRecorderChild(fileAudioInputStream, fileType, wavFile));
    }

    @Override
    public void preStart() {
        logger.info("AudioRecorderChild starting.");

        try {
            AudioSystem.write(fileAudioInputStream, fileType, wavFile);
        } catch(java.io.IOException e) {
            logger.error("java.io.IOException in AudioRecorderChild: [{}].", e.getMessage(),e);
        }
    }


    @Override
    public void postStop() {
        logger.info("AudioRecorderChild ending.");
    }

    public void handleUnknownMessage(Object msg) {
        logger.warn("Received unknown message: [{}]", msg.getClass());
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .matchAny(msg -> handleUnknownMessage(msg))
                .build();
    }
}
