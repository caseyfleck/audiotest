package com.clearcaptions.audio_test.core;

import akka.actor.AbstractActor;
import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.stream.Materializer;
import com.clearcaptions.audio_test.audio.AudioRecorderActor;
import com.clearcaptions.audio_test.audio.AuxiliaryAudioActor;
import com.clearcaptions.audio_test.aws_transcribe.AWSTranscribeActor;
import com.clearcaptions.audio_test.config.AsrConfig;
import com.clearcaptions.audio_test.audio.SpeakerActorSink;
import com.clearcaptions.audio_test.google_stt.GoogleSttActor;
import com.clearcaptions.audio_test.ibm_watson.WatsonActor;
import com.clearcaptions.audio_test.stream.UDPStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class StreamReceivingActor extends AbstractActorWithTimers {

    private static final Logger logger = LoggerFactory.getLogger(StreamReceivingActor.class);

    // Lists all vendors that are launched
    private List<String> captionList;

    private AsrConfig config;

    // The Akka stream object (which accepts the UDP stream from Asterisk) needs access to a materializer. It is advised that the same materializer is used, so for now, we simply pass it on.
    private Materializer materializer;

    // The UDP Stream object; there should only be ONE of these streams established.
    private UDPStream udpStream;

    /**
     * The constructor.
     *
     * @param materializer The Akka stream object (which accepts the UDP stream from Asterisk) needs access to a materializer. It is advised that the same materializer is used, so for now, we simply
     *                     pass it on.
     * @param config This maps to the config object used during the creation of this actor. Note that unlike this object in ASRCaptionSessionManager, this never updates (there is little need for that
     *               as it will only last the length of a call)
     */
    public StreamReceivingActor(Materializer materializer, AsrConfig config) {

        this.config = config;
        this.materializer = materializer;

        this.captionList = new ArrayList<>();

        udpStream = new UDPStream(context().system(), this.materializer, config.getMyIP(), config.getMyPort());

    }

    /**
     * The props.
     *
     */
    public static Props props(Materializer materializer, AsrConfig config) {
        return Props.create(StreamReceivingActor.class, () -> new StreamReceivingActor(materializer, config));
    }

    /*
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     ************************************************************Akka Actor Overrides************************************************************
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     */

    @Override
    public void preStart() {
        //set up the vendors
        setupAndLaunchVendorsAndUdpStream();
    }

    /**
     * This method sets up the ASR vendor actors and then launches them. In addition, this sets up all hashMaps and / or lists that support this ASR Caption Session Actor.
     * Currently, there are two ASR Vendors - here is that list, with the shorthand and longhand reference:
     *  * IBM - IBM Watson
     *  * AWS - AWS Transcribe
     *
     * In addition, this method sets up the Akka UDP Stream from Asterisk, connecting the ASR vendors to the stream as sinks.
     */
    private void setupAndLaunchVendorsAndUdpStream() {

        ActorRef tempActor;

        //we have to build the list of actorRefs that will accept the output of the UDPStream / will act as a sink for the UDPStream
        List<ActorRef> actorsListeningToUDPStream = new ArrayList<>();

        // populate captionList, which tracks all vendors used
        this.captionList = config.getApprovedVendorsAsList();

        //cycle through the list of all vendors we wish to use, either as the main provider OR simply testing it
        for (String vendor: this.captionList) {

            logger.debug("setupVendors: Attempting to launch [{}] actor.", vendor);

            // start the vendor-specific actors, saving their ActorRef to potentialASRVendors; also add the actorRef to actorsListeningToUDPStream which will be used as sinks for the UDP stream
            if (vendor.equals("AWS")) {
                tempActor = context().actorOf(AWSTranscribeActor.props(config.copy()), "my.AWSTranscribeActor");
                actorsListeningToUDPStream.add(tempActor);
            } else if (vendor.equals("IBM")) {
                tempActor = context().actorOf(WatsonActor.props(config.copy()), "my.WatsonActor");
                actorsListeningToUDPStream.add(tempActor);
            } else if (vendor.equals("GOOGLE")) {
                tempActor = context().actorOf(GoogleSttActor.props(config.copy()), "my.GoogleSttActor");
                actorsListeningToUDPStream.add(tempActor);
            } else if (vendor.equals("SPHINX")) {
                //tempActor = context().actorOf(SphinxActor.props(config.copy()), "my.SphinxActor");
                //actorsListeningToUDPStream.add(tempActor);
            } else {
                logger.warn("setupVendors: [{}] not a real vendor - ignoring.", vendor);
            }
        }

        // These two lines add a Speaker actor sink
        tempActor = context().actorOf(SpeakerActorSink.props(config.copy()), "my.SpeakerActor");
        actorsListeningToUDPStream.add(tempActor);

        // This adds the auxiliary audio actor - this actor takes care of things like tone detection etc etc
        tempActor = context().actorOf(AuxiliaryAudioActor.props(config.copy()), "my.AuxiliaryAudioActor");
        actorsListeningToUDPStream.add(tempActor);

        // This adds the AudioRecorderActor
        tempActor = context().actorOf(AudioRecorderActor.props(config.copy()), "my.AudioRecorderActor");
        actorsListeningToUDPStream.add(tempActor);

        // now that all actorRefs that will listen to the stream have been added, actually start the stream IF actors exist (they should)
        if (actorsListeningToUDPStream.size() > 0) udpStream.startUDPStreamWithMultipleActorSinks(actorsListeningToUDPStream);
    }

    public void handleUnknownMessage(Object msg) {
        logger.warn("Received unknown message: [{}]", msg.getClass());
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .matchAny(msg -> handleUnknownMessage(msg))
                .build();
    }
}
