package com.clearcaptions.audio_test;

import akka.actor.ActorSystem;
import akka.stream.Materializer;
import com.clearcaptions.audio_test.config.AsrConfig;
import com.clearcaptions.audio_test.core.StreamReceivingActor;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AudioTest {

    private static final Logger logger = LoggerFactory.getLogger(AudioTest.class);
    private ActorSystem myActorSystem;
    private Materializer myMaterializer;

    private void start(String[] args) {

        Config akkaConfig = ConfigFactory.load().resolve();

        logger.info("Collecting parameters from config file...");
        AsrConfig asrConfig = new AsrConfig(true);

        // If we wish to cluster, ALL cluster nodes MUST use the same Akka actor name
        myActorSystem = ActorSystem.create("AudioTest", akkaConfig);

        myMaterializer = Materializer.createMaterializer(myActorSystem);

        logger.info("Launching the StreamReceivingActor...");
        myActorSystem.actorOf(StreamReceivingActor.props(myMaterializer, asrConfig.copy()), "my.StreamReceivingActor");
    }

    public static void main(String[] args) { new AudioTest().start(args); }
}
