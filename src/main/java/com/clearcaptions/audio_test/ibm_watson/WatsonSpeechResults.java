package com.clearcaptions.audio_test.ibm_watson;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Watson returns a JSON string for speech results. Since we use GSON we need a POJO object - this is that object.
 **/

/*
An example of the Watson JSON string is below, and a description for each field is next to the variable in the class itself:
{
  "results": [
    {
      "final": true,
      "alternatives": [
        {
          "transcript": "I felt the rains down in Africa ",
          "confidence": 0.99,
          "timestamps": [
            [
              "I",
              2.9,
              3.08
            ],
            [
              "bless",
              3.08,
              3.47
            ],
            [
              "the",
              3.47,
              3.57
            ],
            [
              "rains",
              3.57,
              4.12
            ],
            [
              "down",
              4.17,
              4.47
            ],
            [
              "in",
              4.47,
              4.55
            ],
            [
              "Africa",
              4.55,
              5.22
            ]
          ],
          "word_confidence": [
            [
              "I",
              1.0
            ],
            [
              "bless",
              1.0
            ],
            [
              "the",
              1.0
            ],
            [
              "rains",
              1.0
            ],
            [
              "down",
              0.96
            ],
            [
              "in",
              1.0
            ],
            [
              "Africa",
              0.98
            ]
          ]
        }
      ]
    }
  ],
  "result_index": 0
}
 */

public class WatsonSpeechResults {
    private List<InnerResults> results;
    private int result_index;

    public class InnerResults {

        // 'final' refers to if this is the final transmission for the uttered phrase. There are multiple attempts at a translation made of the phrase (all of which are sent),
        // but there is only one 'final' one, which is - in theory - the best translation.
        @SerializedName("final")
        private boolean isFinal;

        // It seems there can be multiple alternatives to a translation - for now we just use one, but that may change in the future (so for now, you will always use
        // 'alternatives.get(0)'), This list contains a bunch of other properties (see embedded class 'WatsonAlternatives')
        public List<WatsonAlternatives> alternatives;

        public class WatsonAlternatives {

            // The transcript is the actual phrase that was transcribed.
            private String transcript;

            // This is the confidence of the overall phrase; 1 is 100% (fully confident)
            private double confidence;

            // This is a double list; the outer list represents a single word in the phrase, in order (for example, there are 7 'timestamps' in the example above). The
            // inner list represents (in this order):
            //      1. The word itself
            //      2. The second (with 0 as the start of the phrase) when the word started
            //      3) the second (with 0 as the start of the phrase) when the word ended.
            //
            // An example would be getTimestamps().get(0).get(2), which would get the endTime (2) of the first (0) timestamp (the value above would be 3.08).
            private List<List<String>> timestamps;

            // This is a double list; the outer list represents a single word in the phrase, in order (for example, there are 7 'word_confidence' in the example above). The
            // inner list represents (in this order):
            //      1. The word itself
            //      2. The confidence of this word (so if this is a 1, IBM was 100% sure the word that was supplied by the transcription was the word said by the speaker).
            //
            // An example would be getWord_confidence().get(6).get(0), which would get the word (0) of the seventh (6) word_confidence (the value above would be 'Africa').
            private List<List<String>> word_confidence;

            public String getTranscript() { return transcript; }
            public void setTranscript(String transcript) { this.transcript = transcript; }

            public double getConfidence() { return confidence; }
            public void setConfidence(double confidence) { this.confidence = confidence; }

            public List<List<String>> getTimestamps() { return timestamps; }
            public void setTimestamps(List<List<String>> timestamps) { this.timestamps = timestamps; }

            public List<List<String>> getWord_confidence() { return word_confidence; }
            public void setWord_confidence(List<List<String>> word_confidence) { this.word_confidence = word_confidence; }

        }

        public boolean getFinal() { return isFinal; }
        public void setFinal(boolean isFinal) { this.isFinal = isFinal; }
    }

    public List<InnerResults> getResults() { return results; }
    public void setResults(List<InnerResults> results) { this.results = results; }

    public int getResult_index() { return result_index; }
    public void setResult_index(int results) { this.result_index = result_index; }
}
