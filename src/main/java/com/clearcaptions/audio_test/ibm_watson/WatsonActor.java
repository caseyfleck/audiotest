package com.clearcaptions.audio_test.ibm_watson;

import akka.actor.*;
import akka.util.ByteString;
import com.clearcaptions.audio_test.audio.Resample;
import com.clearcaptions.audio_test.audio.peers.PcmuDecoder;
import com.clearcaptions.audio_test.audio.peers.PcmuEncoder;
import com.clearcaptions.audio_test.config.AsrConfig;
import com.clearcaptions.audio_test.internal_msgs.ActorSinkMsgs;
import com.ibm.cloud.sdk.core.http.HttpMediaType;
import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.speech_to_text.v1.SpeechToText;
import com.ibm.watson.speech_to_text.v1.model.RecognizeOptions;
import com.ibm.watson.speech_to_text.v1.model.SpeechRecognitionResults;
import com.ibm.watson.speech_to_text.v1.websocket.BaseRecognizeCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import javax.sound.sampled.*;
import java.io.*;
import java.util.Properties;


/**
 * This actor is launched by ASRCaptionSessionActor when a caption session requires an IBM Watson agent.
 *
 * This actor doubles as a sink for the UDP stream - see stream.UDPStream for more information, but this actor is required to respond to the following messages
 *    * ByteString.class (which is the actual payload from the stream)
 *    * ActorSinkMsgs.StreamInitialized.class
 *    * ActorSinkMsgs.StreamCompleted.class
 *    * ActorSinkMsgs.StreamFailure.class
 *
 * This actor sends these messages back to the stream:
 *    * ActorSinkMsgs.Ack.ACKNOWLEDGE - This is sent to the stream as a way of simulating backpressure in the stream; it MUST be done after EVERY message is received from the  stream, otherwise
 *          backpressure will NOT work properly!
 *
 * These messages are received from the parent:
 *   * ASRv2ClusterCommMsg.EndASRCaptionSession - When the ASRCaptionSessionActor is told the caption session is ending, it sends a ASRv2ClusterCommMsg.EndASRCaptionSession message to all its children,
 *          indicating that they should shut down themselves. The child actor responds to the parent with a ASRv2InternalMsg.VendorCaptionEnd (when all vendors respond with a
 *          ASRv2InternalMsg.VendorCaptionEnd, the parent performs QA duties).
 *
 * These messages are sent to its parent ASRCaptionSessionActor:
 *   * ASRv2InternalMsg.VendorCaptionStart - tell the parent ASRCaptionSessionActor that the actor officially launched with its start time.
 *   * ASRv2InternalMsg.VendorCaptionEnd - This is sent to the parent ASRCaptionSessionActor when the vendor session is ending. This is sent in the postStop(), and is in response to a
 *          PoisonPill.getInstance() from the parent ASRCaptionSessionActor. This is important to send to the parent ASRCaptionSessionActor, as this signals to the parent actor that its time to evaluate
 *          the caption quality (the parent must know the child is finished before this can happen).
 *   * ASRv2InternalMsg.BatchCaption - A batch caption is the translated caption received from IBM. When this message is received from IBM, this actor makes a ASRv2InternalMsg.BatchCaption message
 *          and then sends it to its parent ASRCaptionSessionActor (so it can record it for QA and then send the transcribed captions back to the ACD).
 *
 * These messages are sent to the statistic actor(s):
 *   * ASRv2StatisticMsg.IncreaseWatsonActorCountMsg - This message is sent to both the main statistic actor as well as the corresponding Asterisk statistic actor when this actor starts; each
 *          statistic actor increases the associated counter by 1.
 *   * ASRv2StatisticMsg.DecreaseWatsonActorCountMsg - This message is sent to both the main statistic actor as well as the corresponding Asterisk statistic actor when this actor ends; each
 *          statistic actor increases the associated counter by 1.
 *   * ASRv2StatisticMsg.WatsonActorRestart - This message is sent to both the main statistic actor as well as the corresponding Asterisk statistic actor if this actor experiences an error that
 *          forces it to restart; each statistic actor increases the associated counter by 1.
 *
 * These message are sent to itself:
 *   * ReStartSession (Internal class) - This is use to re-start the session after a timeout. WE want to control timeouts internally.
 *          This is done via messaging and NOT recursion simply so the connection can _completely_ clear out and is not left in memory (i.e. the last call to beginCaptioning() is completely cleared
 *          from memory).
 *
 *  This actor also interacts with an IBM object that internally interacts with IBM, sending audio to IBM and receiving captions back in return.
 */

public class WatsonActor extends AbstractActor {

    private static final Logger logger = LoggerFactory.getLogger(WatsonActor.class);


    //This tracks the batch number sent for the captions. only increases by 1 each time a caption is sent
    private int transmissionNum;

    // A properties object is needed because the Watson API Key / URL is in a privileged file - this helps extract that info
    private Properties props;

    // Each sink needs its own PipedOutputStream and PipedInputStream - these actually form a second simple stream (in addition to the target UDP stream), and their purpose is to
    // populate the stream used in an AudioInputStream object. This stream is populated by handleIncomingPacket, and is eventually used as the input to the AudioInputStream.
    private PipedOutputStream pipedOut= new PipedOutputStream();
    private PipedInputStream pipedIn;

    // The audio format this instance will use
    private AudioFormat audioFormat;

    // The audio stream which is sent to Watson
    private AudioInputStream audio;

    // Since this actor will only last a few minutes, this config actor should not change
    private AsrConfig config;

    // because we are dealing with callbacks with IBM, we need to capture the name of this actor in the event Watson responds after the actor finishes (and thus we will not have access to
    // anything about the actor)
    private String myActorName;

    // We want to know when this actor starts - if somethings happen in a short time after this opens we may have to take different actions based on the situation
    private long bornOnTime;

    // We want to keep track of how many buffer dumps happen - if it crosses a certain threshold, we want to be notified
    private long bufferDumps;

    /*
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     ************************************************************CONSTRUCTORS and Props************************************************************
     **********************************************************************************************************************************************
     **********************************************************************************************************************************************
     */

    /**
     * The constructor.
     *
     * @param config The config object.
     */
    public WatsonActor(AsrConfig config) {
        this.config = config;

        props = new Properties();

        this.transmissionNum = 0;
        this.bufferDumps = 0;

        // set the audio format
        //it is important to note that IBM cannot use a sample rate of 8k - it needs at least 16. If it is 8, we must make it 16 here and in the sample rate itself
        int sampleRate = ((this.config.getAudioFormatSampleRate() == 8000)?2:1)*this.config.getAudioFormatSampleRate();
        this.audioFormat = new AudioFormat(sampleRate, this.config.getAudioFormatSampleSizeInBits(), this.config.getAudioFormatChannels(), this.config.getAudioFormatPcmSigned(), this.config.getAudioFormatBigEndian());

        //open this sink's own personal stream for the data.
        try {
            pipedIn = new PipedInputStream(pipedOut, this.config.getAudioFormatInputStreamPipeSize());
        } catch (Exception e) {
            logger.error("Problem adding PipedInputStream. Captions will NOT work for IBM!");
        }

    }

    /**
     * The props
     */
    public static Props props(AsrConfig config) {
        return Props.create(WatsonActor.class, () -> new WatsonActor(config));
    }

    /*
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     ************************************************************Akka Actor Overrides************************************************************
     ********************************************************************************************************************************************
     ********************************************************************************************************************************************
     */

    @Override
    public void preStart() {
        logger.debug("Startup WatsonActor actor with name [{}].", self().path().name());

        myActorName = self().path().name();


        //log in to Watson
        loginToWatson();

        //immediately start captioning!
        this.beginCaptioning();
    }

    @Override
    public void postStop() {


        // close various objects
        try {
            audio.close();
        } catch (Exception e) {
            logger.warn("postStop: could not close audio connection for IBM Watson actor [{}].", self().path().name(), e);
        }
        try {
            pipedIn.close();
        } catch (Exception e) {
            logger.warn("postStop: could not close InputStream pipe for IBM Watson actor [{}].", self().path().name(), e);
        }
        try {
            pipedOut.close();
        } catch (Exception e) {
            logger.warn("postStop: could not close OutputStream pipe for IBM Watson actor [{}].", self().path().name(), e);
        }

        logger.debug("Stopping WatsonActor actor with name [{}].", self().path().name());
    }


    /*
     *************************************************************************************************************************************
     *************************************************************************************************************************************
     ************************************************************Class Methods************************************************************
     *************************************************************************************************************************************
     *************************************************************************************************************************************
     */
    /**
     *
     * This method simply reads the IBM Watson login variables set in ~/.credentials/ibm-watson-credentials.env and then logs in.
     *
     * Here are the usual variables in the .credentials file:
     *
     * SPEECH_TO_TEXT_APIKEY - the API Key for IBM - its probably the same as SPEECH_TO_TEXT_IAM_APIKEY
     * SPEECH_TO_TEXT_IAM_APIKEY - the API Key for IBM - its probably the same as SPEECH_TO_TEXT_APIKEY
     * SPEECH_TO_TEXT_URL - The specific URL this account must use - this is unique per user!
     * SPEECH_TO_TEXT_AUTH_TYPE - The Authentication type (usually 'iam')
     **/
    private void loginToWatson() {

        //we are expecting the Watson credential file to be ~/.credentials/ibm-watson-credentials.env
        //String propFilePathAndName = System.getProperty("user.home") + "/.credentials/ibm-watson-credentials.env";
        String propFilePathAndName = config.getIbmCredentialFile();

        BufferedReader bufferedReader = null;
        logger.debug("loginToWatson: attempting to load IBM Watson password file '{}'.", propFilePathAndName);
        try {
            // look for "~/.credentials/ibm-ibm_watson-credentials.env"
            FileReader fileReader = new FileReader(propFilePathAndName);
            bufferedReader = new BufferedReader(fileReader);
        } catch(Exception e) {
            logger.error("loginToWatson: Could not find file '{}' which holds the IBM Watson credentials - IBM Watson ASR will not be available!", propFilePathAndName);
        }

        try {
            //load the file into the props object and then close bufferedReader
            props.load(bufferedReader);
            bufferedReader.close();
        } catch(Exception e) {
            logger.error("loginToWatson: could not read file '{}' into a properties object; this holds the IBM Watson credentials - IBM Watson ASR will not be available!", propFilePathAndName, e);
        }

    }

    /**
     * This method begins the captioning session with IBM Watson.
     *
     **/
    public void beginCaptioning() {

        RecognizeOptions options = null;

        logger.info("IBM Watson actor [{}] is now providing captions!", self().path().name());

        Authenticator authenticator = new IamAuthenticator(props.getProperty("SPEECH_TO_TEXT_IAM_APIKEY"));

        SpeechToText service = new SpeechToText(authenticator);
        service.setServiceUrl(props.getProperty("SPEECH_TO_TEXT_URL"));

        //it is important to note that IBM cannot use a sample rate of 8k - it needs at least 16. If it is 8, we must make it 16 here and in the sample rate itself
        int sampleRate = ((this.config.getAudioFormatSampleRate() == 8000)?2:1)*this.config.getAudioFormatSampleRate();

        //build the AudioInputStream
        try {
            audio = new AudioInputStream(pipedIn, audioFormat, AudioSystem.NOT_SPECIFIED);
        } catch (Exception e) {
            logger.error("IBM Watson actor [{}] could not open audio input stream. Message: [{}].", self().path().name(), e.getMessage(),e);
        }

        // set the options for this IBM Watson connection
        try {
            options =
                    new RecognizeOptions.Builder()
                            .audio(audio)
                            //if this is true, you will get back partial results. There will be many of these before the phrase is constructed, and for now, we are only worried about the final.
                            // However, and unfortunately, if this is set to false, you will not get back real-time transcriptions (they will all come in later) - so it must be true

                            // sets the end of phrase silence time
                            .endOfPhraseSilenceTime(config.getIbmEndOfPhraseSilenceTime())
                            //sets inactivity timeout. I think its in milliseconds
                            //.inactivityTimeout(1200000)
                            .interimResults(true)
                            .timestamps(true) // gives timestamps for each word.  For now we do not need this, but its available
                            .wordConfidence(true) // tells the confidence behind the word choice.  for now we do not need this but we may in the future
                            .profanityFilter(false) // true means curse words are replaced with ****
                            //.audioMetrics(true) not sure what this is
                            .inactivityTimeout(config.getSilenceTimeout()) // sets inactivity timeout. I think its in milliseconds
                            .contentType(HttpMediaType.AUDIO_RAW + ";rate=" + sampleRate)

                            .build();
        } catch (Exception e) {
            logger.error("IBM Watson actor [{}] could not set its options. Message: [{}].", self().path().name(), e.getMessage(),e);
        }

        // start IBM Watson captioning!
        try {

            service.recognizeUsingWebSocket(
                    options,
                    new BaseRecognizeCallback() {
                        @Override
                        public void onTranscription(SpeechRecognitionResults speechResults) {

                            WatsonSpeechResults mySpeechResults = new Gson().fromJson(speechResults.toString(), WatsonSpeechResults.class);

                            if (mySpeechResults.getResults().get(0).getFinal()) {
                                try {

                                    String transcript = mySpeechResults.getResults().get(0).alternatives.get(0).getTranscript();

                                    // Get the confidence sum = this must be divided by the word count
                                    int wordCount = mySpeechResults.getResults().get(0).alternatives.get(0).getWord_confidence().size();
                                    double tempConfidenceSum = 0;
                                    for (int i = 0;i<wordCount;i++) tempConfidenceSum += Double.parseDouble(mySpeechResults.getResults().get(0).alternatives.get(0).getWord_confidence().get(i).get(1));

                                    /*
                                     Tell the parent of this batch text - from there it will determine what to do.
                                     In addition, we have to check if the context() is null here - since we are using callbacks, its entirely possible that IBM responds with this _after_
                                     the actor shuts down and context() becomes null.
                                     Its OK to dump this message, as either A) the customer has hung up and wouldnt get it anyway or B) the customer has transferred to a human agent
                                     or another ASR captioner. Currently, B) is not an option, so we just dump any last message received.
                                     */
                                    if (context() != null ) {
                                        logger.info("IBM word count: [{}] confidence: [{}] transcript: [{}]", wordCount, (tempConfidenceSum / ((double) wordCount)), transcript);
                                        transmissionNum++;
                                    }

                                } catch (Exception e) {
                                    logger.error("WatsonTransmission: There was an error with the JSON in the WatsonSpeechResults transcription. JSON: [{}]", speechResults.toString(), e);
                                }
                            }
                        }

                        @Override
                        public void onConnected() {
                            logger.debug("WatsonTranscription: IBM Watson transcription connected.");
                        }

                        @Override
                        public void onError(Exception e) {

                        }

                        @Override
                        public void onInactivityTimeout(RuntimeException runtimeException) {

                        }

                        /*
                        // This is onListening() - there is no current need for it to be defined, but its here so we know about it
                        @Override
                        public void onListening() {
                        }
                        */

                        /*
                        // This is onDisconnected() - there is no current need for it to be defined, but its here so we know about it
                        //onTranscriptionComplete() pegs first, then onDisconnected()
                        @Override
                        public void onDisconnected() {

                            logger.error("WatsonTranscription: IBM Watson transcription disconnected on CaptionBridgeID [{}].", captionBridgeID);
                        }
                        */


                        /*
                        // This is onTranscriptionComplete() - there is no current need for it to be defined, but its here so we know about it
                        //onTranscriptionComplete() pegs first, then onDisconnected()
                        @Override
                        public void onTranscriptionComplete() {
                            //logger.debug("WatsonTranscription: IBM Watson transcription complete on CaptionBridgeID [{}].", captionBridgeID);
                        }
                        */
                    });
        } catch (Exception e) {
            logger.error("IBM Watson actor [{}] could not connect with IBM. Message: [{}].", self().path().name(), e.getMessage(),e);
        }

        // At this point, Watson will transcribe anything said; the 'onTranscription' lambda above is used every time a transcription comes back
        // Note the WebSocket MUST be closed when we are finished with it - line.stop() and line.close() takes care of that

    }

    /*
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     ************************************************************Akka Receive Handlers************************************************************
     *********************************************************************************************************************************************
     *********************************************************************************************************************************************
     */

    /**
     * This method simply acts as a receiver for messages we know we will get but do not care to do anything with.
     * @param msg The received message.
     */
    private void doNothing(Object msg) {

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This handles the actual payload from the stream.
     * @param packet A ByteString that is the payload.
     */
    private void handleIncomingPacket(ByteString packet) {

        int inputStreamPipeSize = config.getAudioFormatInputStreamPipeSize();
        byte[] data;
        byte[] rawData = packet.toArray();
        byte[] trashBytes = new byte[inputStreamPipeSize];
        int trashInt;  // not actually used, only set- this is needed if we have to dump a buffer

        //it is important to note that IBM cannot use a sample rate of 8k - it needs at least 16k. If it is 8k, we must make it 16k here and in the audioFormat
        //we can do this by repeating the packet the same number of times of the delta factor - so if we are going from 8k and we want 16k, that is a factor of 2 (doubling)
        int sampleRateModifier = ((this.config.getAudioFormatSampleRate() == 8000)?2:1);

        // we do not have to do anything like ripping rtp headers off or decoding from ulaw to pcmu - we let the asr microservice do this and then it sends the raw data
        data = rawData;

        // IBM needs an upsample - however, for some reason this cannot be done when the audio is in PCMU format; therefore, we change it to ulaw, then upsample, then convert
        // back to PCMU

        //convert to ulaw
        data = PcmuEncoder.process(data);

        // upsample the samples using the repeating method
        data = Resample.upsampleViaRepeating(data, sampleRateModifier);

        // we need to decode from the ulaw codec to the pcmu codec
        data = PcmuDecoder.process(data);

        try {
            // The 'available()' method displays the number of bytes in the pipedIn buffer; if we try to write more data to the buffer than it can hold (capped at inputStreamPipeSize),
            // This will block. we need to completely dump the data if this cap is about to be hit.
            // Technically, the data is read into trashBytes, with trashInt as the number of bytes read - but we just discard them immediately without doing anything with them.
            if ((pipedIn.available() + data.length) > inputStreamPipeSize) {
                trashInt = pipedIn.read(trashBytes);
            }

            pipedOut.write(data);
        } catch (Exception e) { }

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    /**
     * This method is launched if there is an error in the stream.
     * @param failure A throwable failure.
     */
    private void handleStreamError(ActorSinkMsgs.StreamFailure failure) {
        logger.error("Error in stream: [{}].", failure.getCause(), failure);

        //EVERY message MUST be acknowledged - this is the necessary backpressure to receive further messages!
        sender().tell(ActorSinkMsgs.Ack.ACKNOWLEDGE, self());
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(ByteString.class, msg -> handleIncomingPacket(msg))
                .match(ActorSinkMsgs.StreamInitialized.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamCompleted.class, msg -> doNothing(msg))
                .match(ActorSinkMsgs.StreamFailure.class, msg -> handleStreamError(msg))
                .build();
    }

    /*
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     ************************************************************Static Methods************************************************************
     **************************************************************************************************************************************
     **************************************************************************************************************************************
     */

    /**
     * Get the prefix for this type of actor.
     *
     * @return The prefix for this actor.
     */
    public static String getPrefix() { return "ASRIBM"; }

    /**
     * Accepts a caption bridge ID, strips off the captionBridgeID prefix, and then returns an appropriate name for this actor.
     *
     * @param captionBridgeID The associated captionBridge ID in Asterisk for this caption session.
     * @return The proposed name for this actor.
     */
    public static String getProposedActorName(String captionBridgeID) { return getPrefix() + captionBridgeID.substring(2); }
}

